/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe;

import com.esri.arcgis.carto.IMapServerDataAccess;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.interop.extn.ServerObjectExtProperties;
import com.esri.arcgis.server.IServerObjectExtension;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IObjectConstruct;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.IRESTRequestHandler;
import com.esri.arcgis.system.ServerUtilities;
import lu.etat.pch.gis.soe.tasks.EasyPopupTask;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 10/18/12
 * Time: 5:57 AM
 */
@ArcGISExtension
@ServerObjectExtProperties(
        displayName = "EasyPopupSOE",
        description = "EasyPopupSOE",
        allSOAPCapabilities = {"configXML", "layerPopUp"}
//        , supportsMSD = true
)
public class EasyPopupSOE implements IServerObjectExtension, IRESTRequestHandler, IObjectConstruct {
    private static final String TAG = "EasyPopupSOE";
    private IServerObjectHelper soHelper;
    private SOELogger logger;
    private MapServer mapServer = null;
    private IMapServerDataAccess mapServerDataAccess;
    private EasyPopupTask easyPopupTask;

    @Override
    public byte[] handleRESTRequest(String capabilities, String resourceName, String operationName, String operationInput, String outputFormat, String requestProperties, String[] responseProperties) throws IOException {
        logger.debug(TAG, "In handleRESTRequest(): " + ";\ncapabilities: " + capabilities + ";\nresourceName: " + resourceName + ";\noperationName: " + operationName + ";\noperationName length: " + operationName.length() + ";\noperationInput: " + operationInput + ";\noutputFormat: " + outputFormat
                + ";\nresponseProperties length: " + responseProperties.length);
        logger.debug("Operation is permitted", operationName);
        try {
            if (operationName.length() == 0) {
                return "abcdef".getBytes();
            } else {
                JSONObject jsonObject = new JSONObject(operationInput);
                logger.debug(TAG, "handleRESTRequest.jsonObject", jsonObject.toString());

                //bug in 101: https://gist.github.com/3362569
                if (jsonObject.has("f")) outputFormat = jsonObject.getString("f");

                logger.debug(TAG, "handleRESTRequest.jsonObject", jsonObject);
                String answer = null;
                if (operationName.equalsIgnoreCase("configXML")) {
                    String layerIdsTxt = "*";
                    if (jsonObject.has("layerIds")) layerIdsTxt = jsonObject.getString("layerIds");
                    logger.debug(TAG, "handleRESTRequest.configXML", jsonObject);
                    answer = configXML(layerIdsTxt);
                    logger.debug(TAG, "handleRESTRequest.configXML.answer", answer);
                } else if (operationName.equalsIgnoreCase("layerPopUp")) {
                    logger.debug(TAG, "handleRESTRequest.layerPopUp", jsonObject);
                    String layerIdTxt = jsonObject.getString("layerId");
                    try {
                        int layerId = Integer.parseInt(layerIdTxt);
                        logger.debug(TAG, "handleRESTRequest.layerPopUp.layerId", layerId);
                        answer = layerPopUp(layerId);
                        logger.debug(TAG, "handleRESTRequest.layerPopUp.answer", jsonObject);
                    } catch (NumberFormatException nfEx) {
                        logger.error(TAG, "handleRESTRequest.layerId", nfEx);
                    }
                } else {
                    logger.warning(TAG, "Operation is NOT YET IMPLEMENTED: " + operationName + " !!!");
                    answer = RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NOT YET IMPLEMENTED on resource [" + resourceName + "].",
                            new String[]{"details", "operation is not supported"});
                }
                logger.debug(TAG, "handleRESTRequest.outputFormat", outputFormat);
                if (outputFormat.equalsIgnoreCase("xml")) {
                    logger.debug(TAG, "handleRESTRequest.layerPopUp.XML.answer", jsonObject);
                    logger.debug(TAG, "responseProperties", responseProperties);
                    if (responseProperties != null) {
                        logger.debug(TAG, "responseProperties.length", responseProperties.length);
                        logger.debug(TAG, "responseProperties[0]", responseProperties[0]);
                    }
                    JSONObject contentType = new JSONObject();
                    contentType.put("Content-Type", "text/xml");
                    responseProperties[0] = contentType.toString();
                    return answer.getBytes();
                }
                if (outputFormat.equalsIgnoreCase("html")) {
                    logger.debug(TAG, "handleRESTRequest.layerPopUp.html.answer", jsonObject);
                    JSONObject jsonAnswer = new JSONObject();
                    answer = "<textarea cols=\"80\" rows=\"10\">" + answer + "</textarea>";
                    jsonAnswer.put("answer", answer);
                    return jsonAnswer.toString().getBytes();
                } else {
                    JSONObject jsonAnswer = new JSONObject();
                    jsonAnswer.put("answer", answer);
                    return jsonAnswer.toString().getBytes();
                }
            }

        } catch (JSONException e) {
            logger.error(TAG, "handleRESTRequest-JSONException", e);
            return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" ERROR [" + resourceName + "].",
                    new String[]{"JSONException", e.getMessage()}).getBytes();
        }
    }

    private String layerPopUp(int layerId) {
        String answer = easyPopupTask.layerPopUp(layerId);
        return answer.toString();
    }

    private String configXML(String layerIds) {
        String answer = easyPopupTask.configXML(layerIds);
        return answer.toString();
    }

    @Override
    public String getSchema() throws AutomationException {
        logger.debug(TAG, "getSchema()...");
        try {
            JSONObject pchExportSOE = getRootResourceDescription();
            JSONArray operationArray = new JSONArray();
            operationArray.put(RESTUtils.createOperation("configXML", "subLayers", "json,xml"));
            operationArray.put(RESTUtils.createOperation("layerPopUp", "layerId", "json,xml"));
            pchExportSOE.put("operations", operationArray);
            logger.debug(TAG, "getSchema().finished");
            return pchExportSOE.toString();
        } catch (JSONException e) {
            logger.error(TAG, "getSchema().JSONException", e);
        }
        return null;
    }

    private JSONObject getRootResourceDescription() {
        return RESTUtils.createResource("EasyPopupSOE", "EasyPopupSOE, flex popup configs on the fly", false);
    }


    @Override
    public void construct(IPropertySet iPropertySet) throws IOException {
        logger.debug(TAG, "construct...starting");
        easyPopupTask = new EasyPopupTask(logger, mapServer, mapServer.getConfigurationName());
        logger.debug(TAG, "construct ...FINISHED");
    }

    @Override
    public void init(IServerObjectHelper iServerObjectHelper) throws IOException {
        this.soHelper = iServerObjectHelper;
        this.logger = new SOELogger(ServerUtilities.getServerLogger(), 8001);
        logger.debug(TAG, "init...");
        this.mapServer = (MapServer) soHelper.getServerObject();
        logger.debug(TAG, "init.mapServer", mapServer);
        this.mapServerDataAccess = (IMapServerDataAccess) iServerObjectHelper.getServerObject();
        logger.debug(TAG, "init.mapServerDataAccess", mapServerDataAccess);
    }

    @Override
    public void shutdown() throws AutomationException {
        this.soHelper = null;
        this.mapServer = null;
        logger.debug(TAG, "shutdown...");
        this.logger = null;
    }
}
