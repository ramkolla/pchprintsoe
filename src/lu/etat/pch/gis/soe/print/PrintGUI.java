/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.print;

import com.esri.arcgis.beans.TOC.TOCBean;
import com.esri.arcgis.beans.map.MapBean;
import com.esri.arcgis.beans.pagelayout.PageLayoutBean;
import com.esri.arcgis.beans.toolbar.ToolbarBean;
import com.esri.arcgis.carto.MapDocument;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.controls.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.AoInitialize;
import com.esri.arcgis.systemUI.esriCommandStyles;
import lu.etat.pch.gis.soe.tasks.print.PrintTask;
import lu.etat.pch.gis.utils.LicenceUtils;
import lu.etat.pch.gis.utils.SOELogger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 29/03/12
 * Time: 16:16
 */
public class PrintGUI {
    private JButton printButton;
    private JTextArea taInput;
    private JTextArea taOutput;
    private JTextField tfMxdFile;
    public JPanel mainPanel;
    private JTextArea taJson;
    private JButton btJson;
    private JTabbedPane tabbedPane;
    private JPanel mapMainManel;
    private JPanel mapTopPanel;
    private JPanel mapTocPanel;
    private PageLayoutBean pageLayout;
    private MapBean map;
    private TOCBean toc;
    private ToolbarBean toolbar;
    private ToolbarMenu popupMenu;

    private static IServerObjectHelper soHelper;

    public static void main(String[] args) {
        try {
            AoInitialize aoInit = LicenceUtils.initArcEngine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintGUI printGUI = new PrintGUI();

        JFrame myFrame = new JFrame("PchPrintSOE tester");
        printGUI.taInput.setText("{\n" +
                "  \"layoutElements\": [\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": null,\n" +
                "      \"symbol\": {\n" +
                "        \"style\": \"esriSFSSolid\",\n" +
                "        \"color\": {\n" +
                "          \"green\": 218,\n" +
                "          \"alpha\": 255,\n" +
                "          \"red\": 229,\n" +
                "          \"blue\": 190\n" +
                "        },\n" +
                "        \"type\": \"esriSFS\",\n" +
                "        \"outline\": null\n" +
                "      },\n" +
                "      \"xOffset\": null,\n" +
                "      \"name\": \"??? ?? ????? ????\",\n" +
                "      \"yOffset\": null,\n" +
                "      \"geometry\": {\n" +
                "        \"ymin\": 0.075,\n" +
                "        \"ymax\": 29.625,\n" +
                "        \"xmax\": 20.925,\n" +
                "        \"xmin\": 0.075\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 1\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": null,\n" +
                "      \"symbol\": {\n" +
                "        \"border\": {\n" +
                "          \"style\": \"esriSLSSolid\",\n" +
                "          \"color\": {\n" +
                "            \"green\": 0,\n" +
                "            \"alpha\": 255,\n" +
                "            \"red\": 0,\n" +
                "            \"blue\": 0\n" +
                "          },\n" +
                "          \"type\": \"esriSLS\",\n" +
                "          \"width\": 3\n" +
                "        },\n" +
                "        \"rotation\": 0,\n" +
                "        \"mapElements\": null,\n" +
                "        \"type\": \"pchMapFrame\",\n" +
                "        \"referenceScale\": -1,\n" +
                "        \"extent\": null,\n" +
                "        \"mapServices\": null\n" +
                "      },\n" +
                "      \"xOffset\": null,\n" +
                "      \"name\": \"mainMapFrame\",\n" +
                "      \"yOffset\": null,\n" +
                "      \"geometry\": null,\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 5\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": \"bottommid\",\n" +
                "      \"symbol\": {\n" +
                "        \"unitLabelPosition\": null,\n" +
                "        \"unitLabelSymbol\": null,\n" +
                "        \"labelGap\": null,\n" +
                "        \"barColor\": null,\n" +
                "        \"type\": \"pchScaleBar\",\n" +
                "        \"scaleBarType\": \"AlternatingScaleBar\",\n" +
                "        \"barHeight\": 5,\n" +
                "        \"units\": \"km\",\n" +
                "        \"divisions\": null,\n" +
                "        \"divisionsBeforeZero\": null,\n" +
                "        \"labelFrequency\": null,\n" +
                "        \"labelPosition\": null,\n" +
                "        \"division\": null,\n" +
                "        \"backgroundColor\": {\n" +
                "          \"green\": 255,\n" +
                "          \"alpha\": 255,\n" +
                "          \"red\": 255,\n" +
                "          \"blue\": 255\n" +
                "        },\n" +
                "        \"numberFormat\": null,\n" +
                "        \"resizeHint\": null,\n" +
                "        \"labelSymbol\": null,\n" +
                "        \"subdivisions\": null,\n" +
                "        \"unitLabel\": null,\n" +
                "        \"unitLabelGap\": null\n" +
                "      },\n" +
                "      \"xOffset\": 0,\n" +
                "      \"name\": \"????? ???\",\n" +
                "      \"yOffset\": 0,\n" +
                "      \"geometry\": {\n" +
                "        \"y\": 1,\n" +
                "        \"x\": 13\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": \"topleft\",\n" +
                "      \"symbol\": {\n" +
                "        \"numericFormat\": {\n" +
                "          \"showPlusSign\": false,\n" +
                "          \"useSeparator\": false,\n" +
                "          \"zeroPad\": false,\n" +
                "          \"roundingOption\": 0,\n" +
                "          \"roundingValue\": 3,\n" +
                "          \"alignmentOption\": null,\n" +
                "          \"alignmentWidth\": null\n" +
                "        },\n" +
                "        \"pageUnitLabel\": \"\",\n" +
                "        \"type\": \"pchScaleText\",\n" +
                "        \"separator\": \":\",\n" +
                "        \"pageUnits\": \"cm\",\n" +
                "        \"style\": 0,\n" +
                "        \"mapUnitLabel\": \"\",\n" +
                "        \"mapUnits\": \"km\",\n" +
                "        \"backgroundColor\": {\n" +
                "          \"green\": 255,\n" +
                "          \"alpha\": 255,\n" +
                "          \"red\": 255,\n" +
                "          \"blue\": 255\n" +
                "        }\n" +
                "      },\n" +
                "      \"xOffset\": 0,\n" +
                "      \"name\": \"????? ????\",\n" +
                "      \"yOffset\": 0,\n" +
                "      \"geometry\": {\n" +
                "        \"y\": 27.7,\n" +
                "        \"x\": 6\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": \"bottomright\",\n" +
                "      \"symbol\": {\n" +
                "        \"pictureUrl\": \"http://94.23.2.120/insdi_gis/assets/images/mask_b_1.png\",\n" +
                "        \"type\": \"pchPictureElement\"\n" +
                "      },\n" +
                "      \"xOffset\": 0,\n" +
                "      \"name\": \"???\",\n" +
                "      \"yOffset\": 0,\n" +
                "      \"geometry\": {\n" +
                "        \"y\": 1,\n" +
                "        \"x\": 20\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": \"4.95\",\n" +
                "      \"anchor\": \"topleft\",\n" +
                "      \"symbol\": {\n" +
                "        \"pictureUrl\": \"http://94.23.2.120/data/legend/legend300.jpg\",\n" +
                "        \"type\": \"pchPictureElement\"\n" +
                "      },\n" +
                "      \"xOffset\": -5.2,\n" +
                "      \"name\": \"??????? ????\",\n" +
                "      \"yOffset\": -18,\n" +
                "      \"geometry\": {\n" +
                "        \"ymin\": 9.7,\n" +
                "        \"ymax\": 28.7,\n" +
                "        \"xmax\": 4.8,\n" +
                "        \"xmin\": 0.7999999999999998\n" +
                "      },\n" +
                "      \"visible\": true,\n" +
                "      \"width\": \"19\",\n" +
                "      \"order\": 10\n" +
                "    },\n" +
                "    {\n" +
                "      \"height\": null,\n" +
                "      \"anchor\": null,\n" +
                "      \"symbol\": {\n" +
                "        \"border\": {\n" +
                "          \"style\": \"esriSLSSolid\",\n" +
                "          \"color\": {\n" +
                "            \"green\": 0,\n" +
                "            \"alpha\": 255,\n" +
                "            \"red\": 0,\n" +
                "            \"blue\": 0\n" +
                "          },\n" +
                "          \"type\": \"esriSLS\",\n" +
                "          \"width\": 2\n" +
                "        },\n" +
                "        \"rotation\": 0,\n" +
                "        \"mapElements\": null,\n" +
                "        \"type\": \"pchMapFrame\",\n" +
                "        \"referenceScale\": -1,\n" +
                "        \"extent\": null,\n" +
                "        \"mapServices\": null\n" +
                "      },\n" +
                "      \"xOffset\": null,\n" +
                "      \"name\": \"mainMapFrame\",\n" +
                "      \"yOffset\": null,\n" +
                "      \"geometry\": null,\n" +
                "      \"visible\": true,\n" +
                "      \"width\": null,\n" +
                "      \"order\": 1\n" +
                "    }\n" +
                "  ],\n" +
                "  \"mapServices\": [\n" +
                "    {\n" +
                "      \"alpha\": 1,\n" +
                "      \"type\": \"AGS\",\n" +
                "      \"name\": \"Earth Image\",\n" +
                "      \"definitionExpression\": null,\n" +
                "      \"token\": null,\n" +
                "      \"visibleIds\": \"*\",\n" +
                "      \"url\": \"https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"printOutput\": {\n" +
                "    \"format\": \"jpg\",\n" +
                "    \"borderWidth\": [\n" +
                "      6,\n" +
                "      1,\n" +
                "      2,\n" +
                "      1\n" +
                "    ],\n" +
                "    \"pageUnits\": \"cm\",\n" +
                "    \"referenceScale\": 0,\n" +
                "    \"refererWebsite\": \"http://www.web2gis.com\",\n" +
                "    \"border\": null,\n" +
                "    \"exportSettings\": {\n" +
                "      \"exportMeasureInfo\": false,\n" +
                "      \"exportPDFLayersAndFeatureAttributes\": 0,\n" +
                "      \"exportPictureSymbolOptions\": 2,\n" +
                "      \"imageCompression\": 0,\n" +
                "      \"polyginizeMarkers\": false,\n" +
                "      \"compressed\": true,\n" +
                "      \"embedFonts\": true,\n" +
                "      \"colorspace\": 0\n" +
                "    },\n" +
                "    \"resolution\": 160,\n" +
                "    \"mapRotation\": 0,\n" +
                "    \"width\": 21,\n" +
                "    \"toRemoveLayoutElements\": [\"*\"],\n" +
                "    \"height\": 29.7\n" +
                "  },\n" +
                "  \"mapGrids\": [\n" +
                "  ],\n" +
                "  \"mapExtent\": {\n" +
                "    \"ymin\": 3773725.720164609,\n" +
                "    \"spatialReference\": {\"wkid\": 102100},\n" +
                "    \"ymax\": 3853825.720164609,\n" +
                "    \"xmax\": 6149075,\n" +
                "    \"xmin\": 6107075\n" +
                "  },\n" +
                "  \"mapElements\": [],\n" +
                "  \"command\": \"layout\"\n" +
                "}");
        myFrame.getContentPane().add(printGUI.mainPanel);
        myFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        myFrame.setSize(700, 600);
        myFrame.setVisible(true);
        System.out.println("myFrame = " + myFrame);
    }

    public PrintGUI() {
        printButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String mxdFileName = tfMxdFile.getText();
                String input = taInput.getText().trim();
                try {
                    File mxdFile = new File(mxdFileName);
                    if (!mxdFile.canRead()) {
                        MapDocument mapDocument = new MapDocument();
                        File tmpFile = File.createTempFile("PrintSOE_PrintGUI_", ".mxd");
                        tmpFile.deleteOnExit();
                        mxdFileName = tmpFile.toString();
                        System.out.println("creating TEMP mxdDocument for layout= " + mxdFileName);
                        mapDocument.esri_new(mxdFileName);
                        mapDocument.save(false, false);
                        mapDocument.close();
                    }

                    MapServer mapServer = new MapServer();
                    mapServer.connect(mxdFileName);
                    System.out.println("loaded.mxdFileName = " + mxdFileName);
                    String name = mapServer.getDefaultMapName();
                    mapServer.getMap(mapServer.getDefaultMapName()).clearLayers();

                    SOELogger soeLogger = new SOELogger(taOutput, 9000);

                    PrintTask printTask = new PrintTask(soeLogger, mapServer, "printGUI");
                    JSONObject jsonObject;
                    boolean gpTask = true;
                    if (input.contains("handleRESTRequest")) {
                        jsonObject = getParamsFromLog(input);
                        gpTask = false;
                    } else if (input.startsWith("{") && input.endsWith("}")) {
                        jsonObject = new JSONObject(input);
                    } else {
                        jsonObject = getUrlParams(input);
                        gpTask = false;
                    }
                    if (jsonObject.has("input")) {
                        String inputStr = jsonObject.getString("input");
                        System.out.println("input = " + inputStr);
                        inputStr = inputStr.replace("\\\"", "\"");
                        System.out.println("input = " + inputStr);
                        jsonObject = new JSONObject(inputStr);
                    }

                    taJson.setEditable(false);
                    taJson.setText(jsonObject.toString(4));

                    String answer = null;
                    if (jsonObject.getString("command").equals("map"))
                        answer = printTask.printMap(jsonObject, gpTask);
                    else if (jsonObject.getString("command").equals("layout")) {
                        for (int i = 0; i < 1; i++) {
                            answer = printTask.printLayout(jsonObject, gpTask);
                            System.out.println("answer = " + answer);
                        }
                    }
                    System.out.println("answer = " + answer);
                    String newText = taOutput.getText() + "\n\nanswer = " + answer;
                    taOutput.setText(newText);
                    taOutput.setCaretPosition(newText.length());
                    tabbedPane.setSelectedIndex(2);

                    String documentPath = answer + ".mxd";
                    if (pageLayout.checkMxFile(documentPath))
                        pageLayout.loadMxFile(documentPath, null);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        btJson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = taInput.getText().trim();
                JSONObject jsonObject;
                if (input.contains("handleRESTRequest"))
                    jsonObject = getParamsFromLog(input);
                else if (input.startsWith("{") && input.endsWith("}")) {
                    jsonObject = new JSONObject(input);
                } else
                    jsonObject = getUrlParams(input);
                if (jsonObject.has("input")) {
                    String inputStr = jsonObject.getString("input");
                    System.out.println("input = " + inputStr);
                    inputStr = inputStr.replace("\\\"", "\"");
                    System.out.println("input = " + inputStr);
                    jsonObject = new JSONObject(inputStr);
                }

                taJson.setEditable(false);
                taJson.setText(jsonObject.toString(2));
                tabbedPane.setSelectedIndex(1);
            }
        });
        pageLayout = new PageLayoutBean();
        map = new MapBean();
        toc = new TOCBean();
        toolbar = new ToolbarBean();

        mapTopPanel.setLayout(new BorderLayout());
        mapTopPanel.add(toolbar, BorderLayout.CENTER);

        mapMainManel.setLayout(new BorderLayout());
        mapMainManel.add(pageLayout, BorderLayout.CENTER);
        mapTocPanel.setLayout(new BorderLayout());
        mapTocPanel.add(toc, BorderLayout.WEST);

        try {
            toc.setLabelEdit(esriTOCControlEdit.esriTOCControlManual);

            toolbar.addItem(new ControlsOpenDocCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsSelectTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomInTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomOutTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPagePanTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomWholePageCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomPageToLastExtentBackCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsPageZoomPageToLastExtentForwardCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapZoomInTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapZoomOutTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapPanTool(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);
            toolbar.addItem(new ControlsMapFullExtentCommand(), 0, -1, false, 0, esriCommandStyles.esriCommandStyleIconOnly);

            toc.setBuddyControl(pageLayout);
            toolbar.setBuddyControl(pageLayout);
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static JSONObject getParamsFromLog(String logEntry) {
        try {
            int posiStart = logEntry.lastIndexOf("operationInput");
            int posiEnd = logEntry.lastIndexOf("outputFormat");
            String params = logEntry.substring(posiStart + 15, posiEnd - 1);
            params = params.replaceAll("&quot;", "\"");
            JSONObject retObj = new JSONObject(params);
            if (logEntry.contains("operationName: printMap")) retObj.put("command", "map");
            if (logEntry.contains("operationName: printLayout")) retObj.put("command", "layout");
            return retObj;
        } catch (StringIndexOutOfBoundsException ex) {
            //try 10.1
            String params = logEntry.substring(logEntry.indexOf('{'), logEntry.lastIndexOf('}') + 1);
            params = params.replaceAll("&quot;", "\"");
            JSONObject retObj = new JSONObject(params);
            if (logEntry.contains("handleRESTRequest.printMap")) retObj.put("command", "map");
            if (logEntry.contains("handleRESTRequest.printLayout")) retObj.put("command", "layout");
            return retObj;
        }
    }

    private static JSONObject getUrlParams(String url) {
        JSONObject jsonObject = new JSONObject();
        if (url.contains("/printLayout?")) jsonObject.put("command", "layout");
        if (url.contains("/printMap?")) jsonObject.put("command", "map");
        String params = url.substring(url.indexOf("?") + 1);
        StringTokenizer st = new StringTokenizer(params, "&", false);
        while (st.hasMoreTokens()) {
            String param = st.nextToken();
            int posi = param.indexOf("=");
            System.out.println("posi = " + posi);
            String paramName = param.substring(0, posi);
            String paramValue = param.substring(posi + 1);
            System.out.println("paramName = " + paramName);
            System.out.println("paramValue = " + paramValue);
            if (paramName.equals("f") && paramValue.equals("json")) {
                //do nothing, ignore
            } else {
                if (paramValue.startsWith("["))
                    try {
                        jsonObject.put(paramName, new JSONArray(paramValue));
                    } catch (JSONException ex) {
                        System.err.println(paramName + " >> " + ex.getMessage());
                    }
                else {
                    try {
                        jsonObject.put(paramName, new JSONObject(paramValue));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        return jsonObject;
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout(0, 0));
        mainPanel.setEnabled(false);
        tabbedPane = new JTabbedPane();
        mainPanel.add(tabbedPane, BorderLayout.CENTER);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout(0, 0));
        tabbedPane.addTab("INPUT", panel1);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        panel1.add(panel2, BorderLayout.WEST);
        btJson = new JButton();
        btJson.setText("-> JSON");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel2.add(btJson, gbc);
        printButton = new JButton();
        printButton.setEnabled(true);
        printButton.setText("Print");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel2.add(printButton, gbc);
        tfMxdFile = new JTextField();
        tfMxdFile.setText("D:\\arcgisserver_data\\layout\\layoutA4custom.mxd");
        panel1.add(tfMxdFile, BorderLayout.NORTH);
        final JScrollPane scrollPane1 = new JScrollPane();
        panel1.add(scrollPane1, BorderLayout.CENTER);
        taInput = new JTextArea();
        taInput.setLineWrap(true);
        taInput.setToolTipText("http://localhost:6080/arcgis/rest/services/layout/portraitA4/MapServer/exts/PCHPrintSOEv101/printLayout?mapExtent={\"xmin\":49728.36941724636,\"ymin\":57064.783140176034,\"xmax\":109728.36941724636,\"ymax\":143164.78314017603,\"spatialReference\":{\"wkid\":2169}}&printOutput={\"width\":21,\"resolution\":100,\"borderWidth\":[0.5,0.5,0.5,0.5],\"referenceScale\":0,\"format\":\"pdf\",\"mapRotation\":0,\"exportSettings\":null,\"pageUnits\":\"cm\",\"toRemoveLayoutElements\":[\"*\"],\"refererWebsite\":null,\"height\":29.7}&mapElements=[]&layoutElements=[{\"name\":null,\"symbol\":{\"extent\":{\"xmin\":70000,\"ymin\":70000,\"xmax\":80000,\"ymax\":80000},\"type\":\"pchMapFrame\",\"mapServices\":[{\"url\":\"http://localhost:6080/arcgis/rest/services/habitats/MapServer\",\"name\":\"noName\",\"alpha\":1,\"token\":null,\"type\":\"AGS\",\"definitionExpression\":null,\"visibleIds\":\"*\"}],\"mapElements\":[{\"name\":null,\"symbol\":{\"angle\":0,\"style\":\"esriSMSCircle\",\"color\":null,\"size\":15,\"xOffset\":0,\"type\":\"esriSMS\",\"yOffset\":0,\"outline\":null},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":75000,\"y\":75000},\"width\":\"\",\"anchor\":\"bottomleft\",\"height\":\"\"}]},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"xmin\":0.5,\"ymin\":0.5,\"xmax\":10.5,\"ymax\":10.5},\"width\":\"10\",\"anchor\":\"bottomleft\",\"height\":\"10\"},{\"name\":null,\"symbol\":{\"backgroundColor\":{\"blue\":255,\"alpha\":255,\"red\":255,\"green\":255},\"fontFamily\":\"ESRI North\",\"type\":\"pchNorthArrow\",\"size\":48,\"charIndex\":200},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":0.5,\"y\":0.5},\"width\":null,\"anchor\":\"bottomleft\",\"height\":null},{\"name\":null,\"symbol\":{\"textSymbol\":{\"angle\":0,\"yoffset\":0,\"text\":null,\"borderLineColor\":null,\"color\":{\"blue\":0,\"alpha\":255,\"red\":0,\"green\":0},\"backgroundColor\":null,\"type\":\"esriTS\",\"xoffset\":0,\"textFormat\":null},\"text\":\"Title\",\"type\":\"pchTextElement\"},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":10.5,\"y\":29.2},\"width\":null,\"anchor\":\"topmid\",\"height\":null},{\"name\":null,\"symbol\":{\"labelSymbol\":null,\"numberFormat\":null,\"unitLabelPosition\":null,\"resizeHint\":null,\"type\":\"pchScaleBar\",\"barHeight\":5,\"units\":null,\"unitLabelSymbol\":null,\"division\":null,\"unitLabel\":null,\"barColor\":null,\"unitLabelGap\":null,\"scaleBarType\":\"HollowScaleBar\",\"labelPosition\":null,\"divisions\":null,\"labelGap\":null,\"backgroundColor\":{\"blue\":255,\"alpha\":255,\"red\":255,\"green\":255},\"divisionsBeforeZero\":null,\"labelFrequency\":null,\"subdivisions\":null},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":0.5,\"y\":0.5},\"width\":null,\"anchor\":\"bottomleft\",\"height\":null},{\"name\":null,\"symbol\":{\"textSymbol\":{\"angle\":0,\"yoffset\":0,\"text\":null,\"borderLineColor\":null,\"color\":{\"blue\":255,\"alpha\":255,\"red\":255,\"green\":255},\"backgroundColor\":null,\"type\":\"esriTS\",\"xoffset\":0,\"textFormat\":null},\"text\":\"Â© copyright pch\",\"type\":\"pchTextElement\"},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":10.5,\"y\":0.5},\"width\":null,\"anchor\":\"bottommid\",\"height\":null},{\"name\":null,\"symbol\":{\"mapUnitLabel\":\"\",\"pageUnitLabel\":\"\",\"style\":0,\"mapUnits\":\"centimeters\",\"pageUnits\":\"meters\",\"backgroundColor\":{\"blue\":255,\"alpha\":255,\"red\":255,\"green\":255},\"type\":\"pchScaleText\",\"numericFormat\":{\"roundingOption\":0,\"roundingValue\":3},\"separator\":\":\"},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":0.5,\"y\":29.2},\"width\":null,\"anchor\":\"topleft\",\"height\":null},{\"name\":null,\"symbol\":{\"exceptionLayerNamesStr\":\"ortho2010_white,Orthophotos2007,Orthophotos2004,Orthophotos2001,topo_100k,topo_050k,topo_020k,topo_005k\",\"showLabels\":true,\"legendColumns\":1,\"legendBackground\":{\"type\":\"esriSFS\",\"style\":\"esriSFSSolid\",\"color\":{\"blue\":255,\"alpha\":255,\"red\":255,\"green\":255},\"outline\":null},\"showLayerNames\":true,\"legendFormat\":{\"titleGab\":1,\"showTitle\":true,\"titleSymbol\":{\"angle\":0,\"yoffset\":0,\"text\":null,\"borderLineColor\":null,\"color\":{\"blue\":0,\"alpha\":255,\"red\":255,\"green\":0},\"backgroundColor\":null,\"type\":\"esriTS\",\"xoffset\":0,\"textFormat\":{\"style\":\"normal\",\"weight\":\"normal\",\"decoration\":\"none\",\"family\":\"Arial\",\"size\":7,\"color\":null}},\"verticalPatchGap\":0,\"verticalItemGap\":0,\"layerNameGap\":0,\"horizontalItemGap\":0,\"headingGap\":0,\"groupGap\":0,\"defaultPatchWidth\":0,\"defaultPatchHeight\":0,\"horizontalPatchGab\":null,\"textGab\":0.5},\"showHeadings\":false,\"exceptionLayerNames\":[\"ortho2010_white\",\"Orthophotos2007\",\"Orthophotos2004\",\"Orthophotos2001\",\"topo_100k\",\"topo_050k\",\"topo_020k\",\"topo_005k\"],\"type\":\"pchLegend\",\"legendTitle\":\"Legend\",\"includeAllLayers\":true},\"visible\":true,\"xOffset\":0,\"yOffset\":0,\"geometry\":{\"x\":20.5,\"y\":29.2},\"width\":null,\"anchor\":\"topright\",\"height\":null},{\"name\":null,\"symbol\":{\"pictureUrl\":\"http://intranet.pch.etat.lu/files/images/prix_carburants.jpg\",\"type\":\"pchPictureElement\"},\"visible\":true,\"xOffset\":0,\"yOffset\":2,\"geometry\":{\"x\":20.5,\"y\":2.5},\"width\":null,\"anchor\":\"bottomright\",\"height\":null}]&mapServices=[{\"url\":\"http://localhost:6080/arcgis/rest/services/sig/limadm/MapServer\",\"name\":\"LimAdm\",\"alpha\":1,\"token\":\"\",\"type\":\"AGS\",\"definitionExpression\":null,\"visibleIds\":\"1,3,4,6\"}]");
        scrollPane1.setViewportView(taInput);
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new BorderLayout(0, 0));
        tabbedPane.addTab("json", panel3);
        final JScrollPane scrollPane2 = new JScrollPane();
        panel3.add(scrollPane2, BorderLayout.CENTER);
        taJson = new JTextArea();
        taJson.setEditable(true);
        taJson.setLineWrap(true);
        scrollPane2.setViewportView(taJson);
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new BorderLayout(0, 0));
        tabbedPane.addTab("logging", panel4);
        final JScrollPane scrollPane3 = new JScrollPane();
        panel4.add(scrollPane3, BorderLayout.CENTER);
        taOutput = new JTextArea();
        taOutput.setLineWrap(true);
        taOutput.setText("");
        taOutput.setVisible(true);
        taOutput.setWrapStyleWord(false);
        scrollPane3.setViewportView(taOutput);
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new BorderLayout(0, 0));
        tabbedPane.addTab("Map", panel5);
        mapTopPanel = new JPanel();
        mapTopPanel.setLayout(new BorderLayout(0, 0));
        panel5.add(mapTopPanel, BorderLayout.NORTH);
        mapMainManel = new JPanel();
        mapMainManel.setLayout(new GridBagLayout());
        panel5.add(mapMainManel, BorderLayout.CENTER);
        mapTocPanel = new JPanel();
        mapTocPanel.setLayout(new GridBagLayout());
        panel5.add(mapTocPanel, BorderLayout.WEST);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}
