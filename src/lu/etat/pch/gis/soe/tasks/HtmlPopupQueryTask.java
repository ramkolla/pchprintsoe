/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.FeatureLayer;
import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.server.json.JSONException;
import lu.etat.pch.gis.beans.htmlPopup.HtmlPopupQueryResult;
import lu.etat.pch.gis.helper.HtmlPopupHelper;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 12, 2010
 * Time: 6:54:27 PM
 */
public class HtmlPopupQueryTask extends AbstractTask {
    private static final String TAG = "HtmlPopupQueryTask";
    private int layerIndex;
    private String text, objectIds, whereClause, outFields;
    private boolean returnGeometry;

    private File htmlPopupOutputDirectory = null;


    private String[] outFieldNames = new String[0];
    private IGeometry geometry;

    public HtmlPopupQueryTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        try {
            htmlPopupOutputDirectory = getPhysicalMapServiceOutputFolder("htmlPopup");
        } catch (IOException e) {
            logger.error(TAG, "HtmlPopupQueryTask()", e);
        }
    }

    public HtmlPopupQueryResult queryHtmlPopup() throws IOException, JSONException {
        int maxResults = 50;
        logger.debug(TAG, "queryHtmlPopup.layerIndex", layerIndex);
        ILayer layer = this.mapServer.getLayer("", layerIndex);
        logger.debug(TAG, "queryHtmlPopup.layer", layer);
        if (layer == null) return null;
        if (layer instanceof FeatureLayer) {
            FeatureLayer featureLayer = (FeatureLayer) layer;
            //displayFieldProperties(featureLayer.getDisplayFeatureClass());
            logger.debug(TAG, "queryHtmlPopup.featureLayer: " + featureLayer);
            HtmlPopupQueryResult queryResult = new HtmlPopupQueryResult(featureLayer, outFieldNames, returnGeometry);
            queryResult.setOidFieldName(featureLayer.getOIDFieldName());
            IQueryFilter queryFilter;
            if (geometry != null) {
                logger.debug(TAG, "queryHtmlPopup.geometry", AgsJsonGeometry.convertGeomToJson(logger, geometry.getEnvelope()));
                queryFilter = new SpatialFilter();
                ISpatialFilter spatialFilter = (ISpatialFilter) queryFilter;
                spatialFilter.setGeometryByRef(geometry);
                spatialFilter.setGeometryField(featureLayer.getFeatureClass().getShapeFieldName());
                spatialFilter.setSpatialRel(esriSpatialRelEnum.esriSpatialRelIntersects);

            } else {
                queryFilter = new QueryFilter();
            }
            queryFilter.setSubFields(outFields);
            String queryWhereClause = calculateWhereClause(featureLayer);
            queryFilter.setWhereClause(queryWhereClause);
            IFeatureCursor featureCursor = featureLayer.search(queryFilter, true);
            IFeature feature = featureCursor.nextFeature();
            int resultCounter = 0;
            while (feature != null) {
                logger.debug(TAG, "queryHtmlPopup.feature", feature);

                String featureHtmlCode = HtmlPopupHelper.calculateHtmlOutputForFeature(featureLayer, feature, htmlPopupOutputDirectory);
                String pOutput = mapServer.getPhysicalOutputDirectory();
                String vOutput = mapServer.getVirtualOutputDirectory();
                //todo: replace PATH in featureHtmlCode
                //blobFileName = blobFileName.replace(pOutput, vOutput);

                File tmpFile = File.createTempFile("layer_" + layerIndex + "_oid_" + feature.getOID() + "_", ".html", htmlPopupOutputDirectory);
                BufferedWriter buffWriter = new BufferedWriter(new FileWriter(tmpFile));
                buffWriter.write(featureHtmlCode);
                buffWriter.close();
                String htmlPopupUrl = tmpFile.getAbsolutePath();
                htmlPopupUrl = htmlPopupUrl.replace(pOutput, vOutput);
                logger.debug(TAG, "htmlPopupUrl", htmlPopupUrl);
                htmlPopupUrl = htmlPopupUrl.replace("\\", "/");
                logger.debug(TAG, "htmlPopupUrl", htmlPopupUrl);
                queryResult.addFeature(feature, featureHtmlCode, returnGeometry, htmlPopupUrl);
                feature = featureCursor.nextFeature();
                if (resultCounter++ > maxResults) break;

            }
            return queryResult;
        }
        logger.error(TAG, "queryHtmlPopup.NOT a FEATURELayer: ", layer);
        return null;
    }


    private String calculateWhereClause(FeatureLayer featureLayer) throws IOException {
        StringBuffer queryWhereClause = new StringBuffer("1=1");
        logger.debug(TAG, "calculateWhereClause.text", text);
        if (text != null && text.trim().length() > 0) {
            String displayField = featureLayer.getDisplayField();
            queryWhereClause.append(" AND ").append(displayField).append(" like '%").append(text.trim()).append("%'");
        }
        logger.debug(TAG, "calculateWhereClause.whereClause", whereClause);
        if (whereClause != null && whereClause.trim().length() > 0) {
            queryWhereClause.append(" AND ").append(whereClause.trim());
        }
        logger.debug(TAG, "calculateWhereClause.objectIds", objectIds);
        if (objectIds != null && objectIds.trim().length() > 0) {
            objectIds = objectIds.trim();
            String[] objectIdArray = objectIds.split(",");
            if (objectIdArray.length > 0) {
                String oidFieldName = featureLayer.getOIDFieldName();
                queryWhereClause.append(" AND (1=2 OR ");
                for (String objectId : objectIdArray) {
                    queryWhereClause.append(oidFieldName).append(" = ").append(objectId).append(" OR ");
                }
                queryWhereClause.append("2=1)");
            }
        }
        logger.debug(TAG, "calculateWhereClause.queryWhereClause", queryWhereClause);
        return queryWhereClause.toString();
    }

    public int getLayerIndex() {
        return layerIndex;
    }

    public void setLayerId(int layerIndex) {
        this.layerIndex = layerIndex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getObjectIds() {
        return objectIds;
    }

    public void setObjectIds(String objectIds) {
        this.objectIds = objectIds;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    public String getOutFields() {
        return outFields;
    }

    public void setOutFields(String outFields) {
        this.outFields = outFields;
        if (outFields != null) {
            if (outFields.equals("*")) outFieldNames = null;
            else outFieldNames = outFields.toUpperCase().split(",");
        } else {
            outFieldNames = null;
        }
    }

    public boolean isReturnGeometry() {
        return returnGeometry;
    }

    public void setReturnGeometry(boolean returnGeometry) {
        this.returnGeometry = returnGeometry;
    }

    public void setGeometry(IGeometry geometry) {
        this.geometry = geometry;
    }

    public void cleanUpHtmlPopupOutputs() {
        File[] doDeleteFiles = htmlPopupOutputDirectory.listFiles();
        for (File doDeleteFile : doDeleteFiles) {
            logger.debug(TAG, "deleting old htmlPopupFile", doDeleteFile.getName());
            if (!doDeleteFile.delete()) {
                logger.error(TAG, "unable to delete old htmlPopupFile", doDeleteFile.getName());
            }
        }


    }
}
