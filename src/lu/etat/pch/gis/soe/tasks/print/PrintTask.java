/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks.print;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.*;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.ISpatialReference;
import com.esri.arcgis.geoprocessing.GeoProcessor;
import com.esri.arcgis.geoprocessing.IGeoProcessorResult;
import com.esri.arcgis.geoprocessing.tools.datamanagementtools.PackageMap;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.output.*;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.tagRECT;
import lu.etat.pch.gis.soe.tasks.AbstractTask;
import lu.etat.pch.gis.utils.*;
import lu.etat.pch.gis.utils.json.AgsJsonElement;
import lu.etat.pch.gis.utils.json.AgsJsonLayoutElement;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonEnvelope;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonPoint;
import lu.etat.pch.gis.utils.json.layoutElements.AbstractAgsJsonLayoutElement;
import lu.etat.pch.gis.utils.json.layoutElements.AgsJsonMapFrame;
import lu.etat.pch.gis.utils.json.print.*;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Apr 28, 2010
 * Time: 6:01:55 AM
 */
@SuppressWarnings({"AutoBoxing", "ForLoopReplaceableByForEach", "ForeachStatement"})
public class PrintTask extends AbstractTask {
    private static final String TAG = "PrintTask";
    private static final String exportFilePrefix = "PrintTask_";
    private static final boolean GENERATE_ALWAYS_MXD = true;

    private String physicalOutputFolder;
    private String authentication401;
    private MapFrame toRestoreMapFrame;
    MxdUtils mxdUtils = new MxdUtils();


    public PrintTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }

    public void configure401Authentication(String username, String password) {
        String userPassword = username + ":" + password;
        this.authentication401 = "Basic " + new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
        logger.debug(TAG, "PrintTask.configure401Authentication.authentication401: " + authentication401);
    }

    public String printMap(JSONObject jsonObject, boolean gpTask) {
        try {
            JSONObject mapExtent = jsonObject.getJSONObject("mapExtent");
            JSONObject printOutput = jsonObject.getJSONObject("printOutput");
            JSONArray mapElements = jsonObject.optJSONArray("mapElements");
            if (mapElements == null) mapElements = jsonObject.optJSONArray("mapElement");
            if (mapElements == null) mapElements = new JSONArray();
            JSONArray mapServices = (!jsonObject.isNull("mapServices") ? jsonObject.getJSONArray("mapServices") : new JSONArray());
            return print(false, mapExtent, printOutput, mapElements, null, mapServices, null, gpTask);
        } catch (JSONException jsonEx) {
            return RESTUtils.sendError(0, "PrintTask.printMap", new String[]{"JSONException", jsonEx.getMessage()});
        }
    }

    public String printLayout(JSONObject jsonObject, boolean gpTask) {
        try {
            JSONObject mapExtent = jsonObject.getJSONObject("mapExtent");
            JSONObject printOutput = jsonObject.getJSONObject("printOutput");
            JSONArray mapElements = jsonObject.optJSONArray("mapElements");
            if (mapElements == null) mapElements = jsonObject.optJSONArray("mapElement");
            if (mapElements == null) mapElements = new JSONArray();
            JSONArray layoutElements = jsonObject.optJSONArray("layoutElements");
            if (layoutElements == null) layoutElements = jsonObject.optJSONArray("layoutElement");
            if (layoutElements == null) layoutElements = new JSONArray();
            JSONArray mapServices = (!jsonObject.isNull("mapServices") ? jsonObject.getJSONArray("mapServices") : new JSONArray());
            JSONArray mapGrids = (!jsonObject.isNull("mapGrids") ? jsonObject.getJSONArray("mapGrids") : null);
            return print(true, mapExtent, printOutput, mapElements, layoutElements, mapServices, mapGrids, gpTask);
        } catch (JSONException jsonEx) {
            return RESTUtils.sendError(0, "PrintTask.printLayout", new String[]{"JSONException", jsonEx.getMessage()});
        }
    }

    private String print(boolean layout, JSONObject mapExtentJSON, JSONObject printOutputJSON, JSONArray mapElementsJSON, JSONArray layoutElementsArray, JSONArray mapServicesJSON, JSONArray mapGridsArray, boolean gpTask) {
        logger.debug(TAG, "print..........");
        try {
            map.activate(0);
            AgsJsonGeometry jsonGeom = AgsJsonGeometry.convertJsonToAgsJsonGeom(logger, mapExtentJSON);
            IEnvelope mapExtentEnvelope;
            if (jsonGeom instanceof AgsJsonEnvelope) {
                mapExtentEnvelope = ((AgsJsonEnvelope) jsonGeom).toArcObject();
            } else if (jsonGeom instanceof AgsJsonPoint) {
                mapExtentEnvelope = GeometryUtils.convertPoint2Envelope(((AgsJsonPoint) jsonGeom).toArcObject(), 10);
            } else {
                mapExtentEnvelope = jsonGeom.toArcObject().getEnvelope();
            }
            logger.debug(TAG, "print.mapExtentEnvelope", mapExtentEnvelope);
            PchPrintOutput printOutput = new PchPrintOutput(logger, printOutputJSON);
            logger.debug(TAG, "print.printOutput", printOutput);

            List<PchPrintMapService> printMapServices = PchPrintMapService.parseJsonArray(logger, mapServicesJSON);
            logger.debug(TAG, "print.printMapServices.count", printMapServices.size());
            List<AgsJsonElement> mapElementsList = new ArrayList<AgsJsonElement>();
            for (int i = 0; i < mapElementsJSON.length(); i++) {
                mapElementsList.add(new AgsJsonElement(logger, mapElementsJSON.getJSONObject(i)));
            }
            logger.debug(TAG, "print.mapElementsList.size", mapElementsList.size());

            map.setExtent(mapExtentEnvelope);
            List<ILayer> originalMapServiceLayerList = LayerUtils.getMapLayers(logger, map, true);
            logger.debug(TAG, "print.originalMapServiceLayerList.count", originalMapServiceLayerList.size());

            //List<IElement> mapElementsFromJson = AgsJsonElement.getElementsCollectionFromJson(logger, mapElementsJSON, null, printOutput.getResolution());
            logger.debug(TAG, "print.mapElementsFromJson.count", mapElementsList.size());
            List<IElement> addedMapElements = prepareMap(mapExtentEnvelope, printOutput, printMapServices, mapElementsList);
            map.refresh();
            map.contentsChanged();
            List<IElement> addedLayoutElementList = null;
            if (layout) {
                logger.debug(TAG, "print.processingLayout...");
                pageLayout.activate(0);
                prepareScreenDisplay(pageLayout.getScreenDisplay(), printOutput);
                preparePageLayout(printOutput);

                List<AgsJsonLayoutElement> layoutElementsList = new ArrayList<AgsJsonLayoutElement>();
                for (int i = 0; i < layoutElementsArray.length(); i++) {
                    Object layoutElement = layoutElementsArray.get(i);
                    if (layoutElement instanceof JSONObject) {
                        AgsJsonLayoutElement jsonLayoutElement = new AgsJsonLayoutElement(logger, (JSONObject) layoutElement);
                        if (jsonLayoutElement != null)
                            layoutElementsList.add(jsonLayoutElement);
                        else
                            logger.error(TAG, "PrintTask.print.layoutElement.jsonLayoutElement=NULL: " + layoutElement.toString());
                    }
                }
                logger.debug(TAG, "print.layoutElementsList.size", layoutElementsList.size());

                List<AgsJsonMapGrid> mapGrids = AgsJsonMapGrid.parseJsonArray(logger, mapGridsArray);

                map.contentsChanged();
                addedLayoutElementList = configurePageLayoutElements(mapExtentEnvelope, printOutput, layoutElementsList, mapGrids);
                //pageLayout.refreshCaches();
                //pageLayout.contentsChanged();
                logger.debug(TAG, "print.processingLayout.DONE");
            }
            if (printOutput.getScale() != null) {
                logger.debug(TAG, "print.printOutput.getScale", printOutput.getScale());
                map.setMapScale(printOutput.getScale());
                logger.debug(TAG, "print.map.getMapScale", map.getMapScale());
            } else {
                map.setMapScale(printOutput.calculateScale(mapExtentEnvelope, map.getMapUnits()));
                logger.debug(TAG, "print.mapScale.calculated", map.getMapScale());
            }
            String mapServerOutputFolder = getPhysicalOutputFolder();
            if (mapServerOutputFolder == null) mapServerOutputFolder = getPhysicalOutputFolder();
            if (mapServerOutputFolder == null) mapServerOutputFolder = System.getenv("TEMP");
            String fileSeparator = System.getProperty("file.separator");
            if (!mapServerOutputFolder.endsWith(fileSeparator)) mapServerOutputFolder += fileSeparator;
            String exportFileName = mapServerOutputFolder + exportFilePrefix + mapServiceName.replaceAll("/", "_") + "_" + System.currentTimeMillis() + (layout ? "_layout" : "_map") + "." + printOutput.getFormat();
            String outputUrl;
            if (layout) {
                outputUrl = exportViewToFile(exportFileName, printOutput, pageLayout, gpTask);
            } else {
                outputUrl = exportViewToFile(exportFileName, printOutput, map, gpTask);
            }
            if (addedLayoutElementList != null) {
                for (IElement iElement : addedLayoutElementList) {
                    logger.debug(TAG, "print.pageLayout.deleteElement.iElement", iElement);
                    if (iElement != null) {
                        try {
                            pageLayout.deleteElement(iElement);
                        } catch (AutomationException e) {
                            logger.error(TAG, "PrintTask.print.pageLayout.deleteElement[" + iElement + "].AutomationException", e);
                        }
                    }
                }
                pageLayout.refresh();
            }
            if (toRestoreMapFrame != null) {
                pageLayout.addElement(toRestoreMapFrame, 0);
                pageLayout.refresh();
            }
            //cleanup
            for (IElement addedMapElement : addedMapElements) {
                map.getGraphicsContainer().deleteElement(addedMapElement);
            }
            LayerUtils.restoreMapLayers(logger, map, originalMapServiceLayerList, pageLayout);
            map.refresh();
            if (gpTask) {
                return outputUrl;
            }
            JSONObject retObject = new JSONObject();
            retObject.put("outputFile", outputUrl);
            logger.debug(TAG, "PChPrintSOE.printTask.print.outputFile: " + outputUrl);
            return retObject.toString();
        } catch (JSONException e) {
            logger.error(TAG, "PrintTask.print JSONException", e);
            return RESTUtils.sendError(0, "PrintTask.print", new String[]{"JSONException", e.getMessage()});
        } catch (IOException e) {
            logger.error(TAG, "PrintTask.print IOException", e);
            return RESTUtils.sendError(0, "PrintTask.print", new String[]{"IOException", e.getMessage()});
        }
    }

    private void preparePageLayout(PchPrintOutput printOutput) {
        try {
            Page page = (Page) pageLayout.getPage();
            page.setUnits(UnitConverter.getEsriUnitFromString(logger, printOutput.getPageUnits()));
            page.putCustomSize(printOutput.getWidth(), printOutput.getHeight());
            page.setStretchGraphicsWithPage(false);
            pageLayout.reset();
            pageLayout.zoomToWhole();
        } catch (AutomationException e) {
            logger.error(TAG, "preparePageLayout.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "preparePageLayout.IOException", e);
        }
    }

    private List<IElement> prepareMap(IEnvelope mapExtentEnvelope, PchPrintOutput printOutput, List<PchPrintMapService> mapServices, List<AgsJsonElement> mapElements) {
        logger.debug(TAG, "prepareMap...");
        List<IElement> addedMapElements = new ArrayList<IElement>();
        try {
            prepareScreenDisplay(map.getScreenDisplay(), printOutput);
            List<ILayer> toPrintLayerList = PchPrintMapService.createLayersFromPchPrintMapServices(logger, mapServices, printOutput.getRefererWebsite(), map.getSpatialReference(), authentication401);
            logger.debug(TAG, "prepareMap.toPrintLayerList.size", toPrintLayerList.size());
            for (ILayer layerToAdd : toPrintLayerList) {
                if (layerToAdd != null) {
                    logger.debug(TAG, "prepareMap.layerToAdd", layerToAdd.getName());
                    map.addLayer(layerToAdd);
                    logger.debug(TAG, "prepareMap.layerToAdd.OK", layerToAdd.getName());
                    logger.debug(TAG, "prepareMap.layerToAdd.mapLayerSize", map.getLayerCount());
                } else
                    logger.error(TAG, "\n\nPrintTask.prepareMap.layerToAdd = null");
            }
        } catch (AutomationException e) {
            logger.error(TAG, "prepareMap.addLayers.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "prepareMap.addLayers.IOException", e);
        }
        try {
            ISpatialReference spatialReference = mapExtentEnvelope.getSpatialReference();
            if (spatialReference != null) {
                map.setSpatialReferenceByRef(spatialReference);
            }
            map.setExtent(mapExtentEnvelope);

            double referenceScale = printOutput.getReferenceScale();
            logger.debug(TAG, "prepareMap.referenceScale", referenceScale);
            if (referenceScale == -1) {
                referenceScale = map.getMapScale();
                logger.debug(TAG, "prepareMap.referenceScaleCalculated", referenceScale);
            }
            map.setReferenceScale(referenceScale);

            IGraphicsContainer graphicsContainer = map.getGraphicsContainer();
            for (AgsJsonElement mapElement : mapElements) {
                try {
                    List<IElement> toAddMapElements = mapElement.toArcObject(logger);
                    for (IElement toAddMapElement : toAddMapElements) {
                        graphicsContainer.addElement(toAddMapElement, mapElement.getOrder());
                        addedMapElements.add(toAddMapElement);
                    }
                } catch (AutomationException aEx) {
                    logger.error(TAG, "prepareMap.ERROR while adding mapElement: " + mapElement.toJSON());
                }
            }
            map.refresh();
            map.partialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
        } catch (AutomationException e) {
            logger.error(TAG, "prepareMap.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "prepareMap.IOException", e);
        }
        logger.debug(TAG, "prepareMap.DONE");
        return addedMapElements;
    }

    private void prepareScreenDisplay(IScreenDisplay screenDisplay, PchPrintOutput printOutput) throws IOException {
        logger.debug(TAG, "prepareScreenDisplay...");
        IDisplayTransformation displayTransformation = screenDisplay.getDisplayTransformation();

        double widthInPixel = printOutput.getWidthInPixel();
        double heightInPixel = printOutput.getHeightInPixel();
        IEnvelope pBoundsEnvelope = new Envelope();
        pBoundsEnvelope.putCoords(0.0, 0.0, widthInPixel, heightInPixel);

        tagRECT deviceRect = new tagRECT();
        deviceRect.left = 0;
        deviceRect.top = 0;
        deviceRect.right = (int) widthInPixel;
        deviceRect.bottom = (int) heightInPixel;

        logger.debug(TAG, "prepareScreenDisplay.visibleBounds", pBoundsEnvelope);
        displayTransformation.setVisibleBounds(pBoundsEnvelope);
        displayTransformation.setBounds(pBoundsEnvelope);
        logger.debug(TAG, "prepareScreenDisplay.deviceRect", deviceRect);
        displayTransformation.setDeviceFrame(deviceRect);
        logger.debug(TAG, "prepareScreenDisplay.resolution", printOutput.getResolution());
        displayTransformation.setResolution(printOutput.getResolution());
        displayTransformation.setRotation(printOutput.getMapRotation());
        logger.debug(TAG, "prepareScreenDisplay.mapRotation", printOutput.getMapRotation());
        screenDisplay.setDisplayTransformation(displayTransformation);
        logger.debug(TAG, "prepareScreenDisplay.DONE");
    }

    private List<IElement> configurePageLayoutElements(IEnvelope mapExtentEnvelope, PchPrintOutput printOutput, List<AgsJsonLayoutElement> toAddJsonLayoutElements, List<AgsJsonMapGrid> mapGrids) {
        try {
            List<String> toRemoveLayoutElements = printOutput.getToRemoveLayoutElements();
            if (toRemoveLayoutElements.size() > 0) {
                List<IElement> removedElements = new ArrayList<IElement>();
                pageLayout.reset();
                IElement pageElement = pageLayout.next();
                while (pageElement != null) {
                    if (toRemoveLayoutElements.contains("*")) {
                        removedElements.add(pageElement);
                    } else {
                        if (pageElement instanceof MapFrame) {
                            MapFrame mapFrame = (MapFrame) pageElement;
                            if (toRemoveLayoutElements.contains("MapFrame.*") || toRemoveLayoutElements.contains("MapFrame." + mapFrame.getName())) {
                                removedElements.add(pageElement);
                            }
                        } else if (pageElement instanceof MapSurroundFrame) {
                            MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) pageElement;
                            Object mapSurroundFrameObject = mapSurroundFrame.getObject();
                            if (mapSurroundFrameObject instanceof MarkerNorthArrow) {
                                MarkerNorthArrow markerNorthArrow = (MarkerNorthArrow) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("NorthArrow.*") || toRemoveLayoutElements.contains("NorthArrow." + markerNorthArrow.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else if (mapSurroundFrameObject instanceof IScaleBar) {
                                IScaleBar scaleBar = (IScaleBar) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("ScaleBar.*") || toRemoveLayoutElements.contains("ScaleBar." + scaleBar.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else if (mapSurroundFrameObject instanceof ScaleText) {
                                ScaleText scaleText = (ScaleText) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("ScaleText.*") || toRemoveLayoutElements.contains("ScaleText." + scaleText.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else if (mapSurroundFrameObject instanceof Legend) {
                                Legend legend = (Legend) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("Legend.*") || toRemoveLayoutElements.contains("Legend." + legend.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else {
                                logger.error(TAG, "\n\n\n\nUnHandled MapSurroundFrameObject in PrintTask.mapSurroundFrameObject=" + mapSurroundFrameObject.getClass() + "  > " + mapSurroundFrameObject);
                            }
                        } else if (pageElement instanceof TextElement) {
                            TextElement textElement = (TextElement) pageElement;
                            if (toRemoveLayoutElements.contains("TextElement.*") || toRemoveLayoutElements.contains("TextElement." + textElement.getName())) {
                                removedElements.add(pageElement);
                            }
                        } else {
                            logger.error(TAG, "\nPrintTask.nonMapFrameElement found: " + pageElement.getClass() + " >> " + pageElement);
                        }
                    }
                    pageElement = pageLayout.next();
                }
                for (IElement toRemoveElement : removedElements) {
                    if (toRemoveElement instanceof IElementProperties) {
                        logger.debug(TAG, "removing existing layoutElement", toRemoveElement + "." + ((IElementProperties) toRemoveElement).getName());
                    } else if (toRemoveElement instanceof MapSurroundFrame) {
                        MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) pageElement;
                        logger.debug(TAG, "removing existing layoutElement", mapSurroundFrame.getObject() + "." + mapSurroundFrame.getName());
                    } else {
                        logger.debug(TAG, "removing existing layoutElement", toRemoveElement + " !!??!!");
                    }
                    pageLayout.deleteElement(toRemoveElement);
                }
                pageLayout.refresh();
            }
        } catch (AutomationException e) {
            logger.error(TAG, "PrintTask.configurePageLayoutElements", e);
        } catch (IOException e) {
            logger.error(TAG, "PrintTask.configurePageLayoutElements", e);
        }
        try {
            IEnvelope mapFrameEnv = printOutput.calculateMapFrameEnvelope();
            logger.debug(TAG, "configurePageLayoutElements.map", map);
            logger.debug(TAG, "configurePageLayoutElements.mapFrameEnv", mapFrameEnv);
            logger.debug(TAG, "configurePageLayoutElements.pageLayout", pageLayout);
            boolean mapAdded = false;
            MapFrame mapFrame = (MapFrame) pageLayout.findFrame(map);
            toRestoreMapFrame = mapFrame; //saved to add it back at the end
            logger.debug(TAG, "configurePageLayoutElements.mapFrame", mapFrame);
            if (mapFrame == null) {
                logger.debug(TAG, "configurePageLayoutElements.preNewMapFrame()");
                mapFrame = new MapFrame();
                logger.debug(TAG, "configurePageLayoutElements.mapFrame1", mapFrame);
                mapFrame.setMapByRef(map);
                logger.debug(TAG, "configurePageLayoutElements.mapFrame2", mapFrame);
                mapFrame.setGeometry(mapFrameEnv);
                mapAdded = true;
            }
            logger.debug(TAG, "configurePageLayoutElements.mapAdded", mapAdded);
            logger.debug(TAG, "configurePageLayoutElements.0.map", map);
            logger.debug(TAG, "configurePageLayoutElements.0.mapFrame", mapFrame);
            map.setExtent(mapExtentEnvelope);
            map.refresh();
            logger.debug(TAG, "configurePageLayoutElements.mapAdded", mapAdded);
            logger.debug(TAG, "configurePageLayoutElements.1.map", map);
            logger.debug(TAG, "configurePageLayoutElements.1.mapFrame", mapFrame);
            if (printOutput.getScale() != null) {
                map.setMapScale(printOutput.getScale());
                logger.debug(TAG, "configurePageLayoutElements.mapScale.fromRest", map.getMapScale());
            } else {
                map.setMapScale(printOutput.calculateScale(mapExtentEnvelope, map.getMapUnits()));
                logger.debug(TAG, "configurePageLayoutElements.mapScale.calculated", map.getMapScale());
            }
            logger.debug(TAG, "configurePageLayoutElements.2.map", map);
            logger.debug(TAG, "configurePageLayoutElements.2.mapFrame", mapFrame);
            map.refresh();
            logger.debug(TAG, "configurePageLayoutElements.3.map", map);
            logger.debug(TAG, "configurePageLayoutElements.3.mapFrame", mapFrame);

            logger.debug(TAG, "configurePageLayoutElements.mapGrids.length", mapGrids.size());
            for (AgsJsonMapGrid agsJsonMapGrid : mapGrids) {
                IMapGrid mapGrid = agsJsonMapGrid.toArcObject();
                if (mapGrid != null) {
                    logger.debug(TAG, "configurePageLayoutElements.mapGrids.add", agsJsonMapGrid);
                    mapFrame.addMapGrid(mapGrid);
                }
            }
            logger.debug(TAG, "configurePageLayoutElements.mapGrids.END");

            boolean mainMapFrameFound = false;
            for (AgsJsonLayoutElement toAddJsonLayoutElement : toAddJsonLayoutElements) {
                if (toAddJsonLayoutElement.getLayoutElement() instanceof AgsJsonMapFrame) {
                    if (toAddJsonLayoutElement.getName().equalsIgnoreCase("mainMapFrame")) {
                        mainMapFrameFound = true;
                        break;
                    }
                }
            }
            if (!mainMapFrameFound) {
                AgsJsonMapFrame jsonMapFrame = new AgsJsonMapFrame(logger);
                toAddJsonLayoutElements.add(new AgsJsonLayoutElement("mainMapFrame", new AgsJsonEnvelope(logger, mapFrameEnv), jsonMapFrame));
            }

            List<IElement> toAddLayoutElements = new ArrayList<IElement>();
            Collections.sort(toAddJsonLayoutElements, new Comparator<AgsJsonLayoutElement>() {
                @Override
                public int compare(AgsJsonLayoutElement o1, AgsJsonLayoutElement o2) {
                    int order1 = o1.getOrder();
                    int order2 = o2.getOrder();
                    if (order1 == 0 && order2 == 0) {
                        String o1Type = o1.getLayoutElement().getType();
                        String o2Type = o2.getLayoutElement().getType();
                        if (o1Type.equals(o2Type)) {
                            if (o1.getName().equalsIgnoreCase("mainMapFrame")) return -1;
                            if (o2.getName().equalsIgnoreCase("mainMapFrame")) return 1;
                        }
                        if (o1Type.equals(AgsJsonMapFrame.TYPE)) return -1;
                        if (o2Type.equals(AgsJsonMapFrame.TYPE)) return 1;
                    }
                    return (order1 - order2);
                }
            });

            for (AgsJsonLayoutElement toAddJsonLayoutElement : toAddJsonLayoutElements) {
                List<IElement> layoutElements = new ArrayList<IElement>();
                AbstractAgsJsonLayoutElement tmpLayoutElement = toAddJsonLayoutElement.getLayoutElement();
                if (tmpLayoutElement != null && tmpLayoutElement instanceof AgsJsonMapFrame
                        && toAddJsonLayoutElement.getName().equalsIgnoreCase("mainMapFrame")) {
                    AgsJsonMapFrame mainMapFrame = (AgsJsonMapFrame) toAddJsonLayoutElement.getLayoutElement();
                    mainMapFrame.processMainMapFrame(mapFrame, map);
                    layoutElements.add(mapFrame);
                    logger.debug(TAG, "configurePageLayoutElements.mainMapFrame.END");
                } else {
                    try {
                        layoutElements.addAll(toAddJsonLayoutElement.toArcObject(logger, pageLayout, mapFrame));
                    } catch (Exception ex) {
                        logger.error(TAG, "configurePageLayoutElements.layerElement.toArcObjects.Exception", ex);
                    }
                }
                toAddLayoutElements.addAll(layoutElements);
            }
            for (IElement toAddElement : toAddLayoutElements) {
                if (toAddElement == null) {
                    logger.error(TAG, "configurePageLayoutElements.toAddElement = null !!");
                } else {
                    if (toAddElement.getGeometry() == null) {
                        logger.error(TAG, "configurePageLayoutElements.toAddElement.GEOMETRY.isNULL = " + toAddElement);
                    } else {
                        if (toAddElement instanceof IMapSurround)
                            ((IMapSurround) toAddElement).setMapByRef(map);
                        try {
                            logger.debug(TAG, "configurePageLayoutElements.toAddElement", toAddElement);
                            pageLayout.addElement(toAddElement, 0);
                        } catch (AutomationException e) {
                            logger.error(TAG, "configurePageLayoutElements.AutomationException (" + toAddElement + ")", e);
                        }
                    }
                }
            }
            map.setMapScale(printOutput.calculateScale(mapExtentEnvelope, map.getMapUnits()));
            pageLayout.partialRefresh(esriViewDrawPhase.esriViewAll, null, null);
            pageLayout.partialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
            return toAddLayoutElements;
        } catch (AutomationException e) {
            logger.error(TAG, "configurePageLayoutElements.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "configurePageLayoutElements.IOException", e);
        }
        return null;
    }

    private String exportViewToFile(String exportFileName, PchPrintOutput printOutput, IActiveView activeView, boolean gpTask) throws IOException {
        String mxdFileName = "?";
        if (GENERATE_ALWAYS_MXD || printOutput.getFormat().equalsIgnoreCase("mxd")) {
            try {
                if (activeView instanceof Map) {
                    mxdFileName = mxdUtils.exportMapToMxdFile(logger, exportFileName, (Map) activeView);
                } else if (activeView instanceof PageLayout) {
                    mxdFileName = mxdUtils.exportLayoutToMxdFile(logger, exportFileName, (PageLayout) activeView);
                } else {
                    logger.error(TAG, "\n\n\n\nUnHandled exportViewToFile on IActiveView: " + activeView);
                    return mxdFileName;
                }
            } catch (Exception ex) {
                logger.error(TAG, "PrintTask.exportViewToFile.generateMXD.Exception", ex);
                ex.printStackTrace();
            }
        }
        logger.debug(TAG, "exportViewToFile.mxdFileName", mxdFileName);
        if (printOutput.getFormat().equalsIgnoreCase("mxd")) {
            exportFileName = mxdFileName;
        } else if (printOutput.getFormat().equalsIgnoreCase("mpk")) {
            logger.debug(TAG, "exportViewToFile.mkp.exportFileName", exportFileName);
            PackageMap packageMapTool = new PackageMap(mxdFileName, exportFileName);
            packageMapTool.setConvertData("CONVERT");
            packageMapTool.setConvertArcsdeData("CONVERT_ARCSDE");
            if (activeView instanceof PageLayout) {
                PageLayout pageLayout = (PageLayout) activeView;
                IEnvelope env = ((Map) pageLayout.getFocusMap()).getExtent();
                String extentStr = env.getXMin() + " " + env.getYMin() + " " + env.getXMax() + " " + env.getYMax();
                packageMapTool.setExtent(extentStr);
            } else {
                IEnvelope env = activeView.getExtent();
                String extentStr = env.getXMin() + " " + env.getYMin() + " " + env.getXMax() + " " + env.getYMax();
                packageMapTool.setExtent(extentStr);
            }
            packageMapTool.setApplyExtentToArcsde("ALL");
            GeoProcessor geoProcessor = new GeoProcessor();
            IGeoProcessorResult result = geoProcessor.execute(packageMapTool, null);
            logger.debug(TAG, "exportViewToFile.mpk", result.getReturnValue());
        } else if (!printOutput.getFormat().equalsIgnoreCase("mxd")) {
            IExport export = null;
            AbstractPchPrintExportSettings exportSettings = printOutput.getExportSettings();
            if (exportSettings != null) {
                export = exportSettings.toArcObject();
            } else {
                if (printOutput.getFormat().equalsIgnoreCase("pdf")) {
                    ExportPDF exportPDF = new PchPrintExportPDF().toArcObject();
                    exportPDF.setImageCompression(esriExportImageCompression.esriExportImageCompressionLZW);
                    logger.debug(TAG, "exportViewToFile.exportJPEG", exportPDF);
                    export = exportPDF;
                } else if (printOutput.getFormat().equalsIgnoreCase("jpeg") || printOutput.getFormat().equalsIgnoreCase("jpg")) {
                    ExportJPEG exportJPEG = new ExportJPEG();
                    logger.debug(TAG, "exportViewToFile.exportJPEG", exportJPEG);
                    exportJPEG.setProgressiveMode(true);
                    exportJPEG.setQuality((short) 100);
                    exportJPEG.setImageType(esriExportImageType.esriExportImageTypeTrueColor);
                    export = exportJPEG;
                } else if (printOutput.getFormat().equalsIgnoreCase("gif")) {
                    ExportGIF exportGIF = new ExportGIF();
                    logger.debug(TAG, "exportViewToFile.exportGIF", exportGIF);
                    export = exportGIF;
                } else if (printOutput.getFormat().equalsIgnoreCase("bmp")) {
                    ExportBMP exportBMP = new ExportBMP();
                    logger.debug(TAG, "exportViewToFile.exportBMP", exportBMP);
                    export = exportBMP;
                } else if (printOutput.getFormat().equalsIgnoreCase("png")) {
                    ExportPNG exportPNG = new ExportPNG();
                    logger.debug(TAG, "exportViewToFile.exportPNG", exportPNG);
                    export = exportPNG;
                } else if (printOutput.getFormat().equalsIgnoreCase("emf")) {
                    ExportEMF exportEMF = new ExportEMF();
                    logger.debug(TAG, "exportViewToFile.exportEMF", exportEMF);
                    export = exportEMF;
                } else if (printOutput.getFormat().equalsIgnoreCase("tif") || printOutput.getFormat().equalsIgnoreCase("tifw")) {
                    ExportTIFF exportTIFF = new ExportTIFF();
                    logger.debug(TAG, "exportViewToFile.exportTIFF", exportTIFF);
                    if (printOutput.getFormat().equalsIgnoreCase("tifw")) {
                        exportTIFF.setGeoTiff(true);
                        exportTIFF.setMapExtent(activeView.getExtent());
                        exportFileName = exportFileName.substring(0, exportFileName.length() - 1);
                    }
                    export = exportTIFF;
                }
            }
            if (export != null) {
                logger.debug(TAG, "exportViewToFile.exporting.exportFileName", exportFileName);
                export.setExportFileName(exportFileName);

                double outputResolution = printOutput.getResolution();
                export.setResolution(outputResolution);
                configureRasterOutputResampleRation(activeView, esriResampleRatio.esriRasterOutputBest);

                if (export instanceof IOutputRasterSettings)
                    ((IOutputRasterSettings) export).setResampleRatio(esriResampleRatio.esriRasterOutputBest);

                tagRECT exportRect = new tagRECT();
                exportRect.left = 0;
                exportRect.top = 0;
                exportRect.right = (int) (printOutput.getWidthInPixel());
                exportRect.bottom = (int) (printOutput.getHeightInPixel());

                Envelope pPixelBoundsEnv = new Envelope();
                pPixelBoundsEnv.putCoords(exportRect.left, exportRect.top, exportRect.right, exportRect.bottom);
                export.setPixelBounds(pPixelBoundsEnv);

                int hDC = export.startExporting();
                if (activeView instanceof PageLayout) {
                    logger.debug(TAG, "exportViewToFile.output.pageLayout...");
                    activeView.output(hDC, (int) outputResolution, exportRect, null, null);
                } else {
                    logger.debug(TAG, "exportViewToFile.output.map... ");
                    activeView.output(hDC, (int) outputResolution, exportRect, activeView.getExtent(), null);
                }
                export.finishExporting();
                logger.debug(TAG, "exportViewToFile.output.FINISHED");
                export.cleanup();
            }
        }
        if (!gpTask) {
            String pOutput = getPhysicalOutputFolder();
            logger.debug(TAG, "exportViewToFile.pOutput", pOutput);
            String vOutput = getVirtualMapServiceOutputFolder(null);
            logger.debug(TAG, "exportViewToFile.vOutput", vOutput);
            if ((pOutput.endsWith("\\") || pOutput.endsWith("/")) && !vOutput.endsWith("/")) {
                vOutput = vOutput + "/";
            }
            String newExportFileName = exportFileName.replace(pOutput, vOutput);
            logger.debug(TAG, "exportViewToFile.newExportFileName1", newExportFileName);
            newExportFileName = newExportFileName.replaceAll("\\\\", "/");
            logger.debug(TAG, "exportViewToFile.newExportFileName2", newExportFileName);
            return newExportFileName;
        }
        return exportFileName;
    }


    private void configureRasterOutputResampleRation(IActiveView activeView, int resampleRation) throws IOException {
        IScreenDisplay screenDisplay = activeView.getScreenDisplay();
        DisplayTransformation displayTransformation = (DisplayTransformation) screenDisplay.getDisplayTransformation();
        displayTransformation.setResampleRatio(resampleRation);
        screenDisplay.setDisplayTransformation(displayTransformation);
        if (activeView instanceof PageLayout) {
            PageLayout pageLayout = (PageLayout) activeView;
            pageLayout.reset();
            IElement layoutElem;
            while ((layoutElem = pageLayout.next()) != null) {
                if (layoutElem instanceof IMapFrame) {
                    IMapFrame mapFrame = (IMapFrame) layoutElem;
                    Map layoutMap = (Map) mapFrame.getMap();
                    configureRasterOutputResampleRation(layoutMap, resampleRation);
                }
            }
        }
    }

    public void cleanupOldExports() throws IOException {
        final File exportFileDirectory = new File(getPhysicalOutputFolder());
        logger.debug(TAG, "cleanupOldExports.exportFileDirectory", exportFileDirectory.toString());
        final String cleanMapServiceName = (exportFilePrefix + mapServiceName.replaceAll("/", "_")).toLowerCase();
        logger.debug(TAG, "cleanupOldExports.exportFileDirectory", cleanMapServiceName);
        File[] doDeleteFiles = exportFileDirectory.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().startsWith(exportFilePrefix + "enginetest_")
                        || name.toLowerCase().startsWith(cleanMapServiceName + "_");
            }
        });
        for (File doDeleteFile : doDeleteFiles) {
            if (!doDeleteFile.delete()) {
                logger.error(TAG, "PrintTask.cleanupOldExports.unable to delete old exportFile: " + doDeleteFile.getName());
            }
        }
    }

    public void setPhysicalOutputFolder(String physicalOutputFolder) {
        this.physicalOutputFolder = physicalOutputFolder;
    }

    public String getPhysicalOutputFolder() throws IOException {
        if (physicalOutputFolder == null)
            return getPhysicalMapServiceOutputFolder(null).toString();
        File physicalOutputFolderFile = new File(physicalOutputFolder);
        if (!physicalOutputFolderFile.exists())
            physicalOutputFolderFile.mkdirs();
        return physicalOutputFolder;
    }
}