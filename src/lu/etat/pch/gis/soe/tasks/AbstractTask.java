/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.*;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 6, 2010
 * Time: 6:06:39 PM
 */
public class AbstractTask {
    private static final String TAG = "AbstractTask";
    public SOELogger logger;
    protected MapServer mapServer;
    public String mapServiceName;

    //new for 10.1
    private String requestContextURL;

    protected Map map;
    protected PageLayout pageLayout;

    private static final String[] possibleArcgisServerHomes = {
            "C:\\Program Files\\ArcGIS\\server\\user\\cfg",
            "C:\\Program Files\\ArcGIS\\Server9.4\\server\\user\\cfg",
            "C:\\Program Files\\ArcGIS\\Server9.4\\server\\user\\cfg",
            "C:\\Program Files\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "C:\\Program Files (x86)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "D:\\Program Files\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "D:\\Program Files (x86)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "E:\\Program Files)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "E:\\Program Files (x86)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "/data/arcgis/server9.4/server/user/cfg",
            "/opt/arcgis/server9.4/server/user/cfg",
            "/opt/arcgis/server10.0/server/user/cfg",
            "/opt/arcgis/server10.1/server/user/cfg"
    };

    public AbstractTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        this.logger = logger;
        this.mapServer = mapServer;
        this.mapServiceName = mapServiceName;
        initMapPageLayout(mapServer);
    }

    public File getPhysicalMapServiceOutputFolder(String subFolder) throws IOException {
        if (mapServer == null) {
            return File.listRoots()[0];
        }
        logger.debug(TAG, "getPhysicalMapServiceOutputFolder.start...");
        String mapServerOutputFolder = getMapServerOutputFolder(mapServer);
        logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServerOutputFolder: " + mapServerOutputFolder);
        String mapServiceOutputFolder = mapServerOutputFolder;
        if (mapServerOutputFolder.endsWith("_MapServer") || mapServerOutputFolder.endsWith("_MapServer/") || mapServerOutputFolder.endsWith("_MapServer\\")) {
            //do nothing, already mapserviceName included
        } else {
            logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceName: " + mapServiceName);
            String mapServiceSuffix = mapServiceName.replace("/", "_") + "_MapServer";
            if (mapServiceOutputFolder.endsWith("\\")) {
                mapServiceOutputFolder = mapServiceOutputFolder.substring(0, mapServiceOutputFolder.length() - 1);
            }
            if (mapServiceSuffix.indexOf('_') != mapServiceSuffix.lastIndexOf('_')) {
                //has folder/mapService structure
                String msFolder = mapServiceSuffix.substring(mapServiceSuffix.indexOf('_') + 1);
                logger.debug(TAG, "getPhysicalMapServiceOutputFolder.msFolder: " + msFolder);
                if (!mapServerOutputFolder.endsWith(msFolder) && !mapServerOutputFolder.substring(0, mapServiceOutputFolder.length() - 1).endsWith(msFolder)) {
                    mapServiceOutputFolder = mapServerOutputFolder + mapServiceSuffix;
                    logger.debug(TAG, "getPhysicalMapServiceOutputFolder1a.mapServiceOutputFolder: " + mapServiceOutputFolder);
                }
                logger.debug(TAG, "getPhysicalMapServiceOutputFolder1b.mapServiceOutputFolder: " + mapServiceOutputFolder);
            } else {
                logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceSuffix: " + mapServiceSuffix);
                logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceOutputFolder: " + mapServiceOutputFolder);
                if (!mapServerOutputFolder.endsWith(mapServiceSuffix)) {
                    mapServiceOutputFolder = mapServerOutputFolder + mapServiceSuffix;
                }
                logger.debug(TAG, "getPhysicalMapServiceOutputFolder2.mapServiceOutputFolder: " + mapServiceOutputFolder);
            }
        }
        File mapServiceOutputFolderFile = new File(mapServiceOutputFolder);
        if (!mapServiceOutputFolderFile.exists())
            mapServiceOutputFolderFile.mkdirs();
        logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceOutputFolder9", mapServiceOutputFolder);
        logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceOutputFolder.read:", mapServiceOutputFolderFile.canRead());
        logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceOutputFolder.write", mapServiceOutputFolderFile.canWrite());
        if (subFolder != null && subFolder.trim().length() > 0) {
            logger.debug(TAG, "getPhysicalMapServiceOutputFolder.mapServiceOutputFolder.subFolder: " + subFolder);
            File msSubFolder = new File(mapServiceOutputFolder, subFolder);
            logger.debug(TAG, "getPhysicalMapServiceOutputFolder.msSubFolder: " + msSubFolder);
            if (msSubFolder.exists()) {
                return msSubFolder;
            } else if (msSubFolder.mkdirs()) {
                return msSubFolder;
            }
        }
        return mapServiceOutputFolderFile;
    }

    public String getVirtualMapServiceOutputFolder(String subFolder) {
        if (mapServer == null) {
            return "??";
        }
        try {
            String mapServerOutputFolder = mapServer.getVirtualOutputDirectory();
            if (mapServerOutputFolder.toLowerCase().startsWith("http") || mapServerOutputFolder.toLowerCase().startsWith("https")) {
                //everything is ok
            } else {
                logger.debug(TAG, "getVirtualMapServiceOutputFolder.needs101FIX: " + mapServerOutputFolder);
                String base101Url = getRequestContextURL();
                logger.error(TAG, "getVirtualMapServiceOutputFolder: >" + getRequestContextURL() + "<");
                if (mapServerOutputFolder.equals("/rest/directories/PRINTSOE/layout_MapServer/")) {
                    mapServerOutputFolder = "/rest/directories/arcgisoutput/PRINTSOE/layout_MapServer/";
                }
                mapServerOutputFolder = base101Url + mapServerOutputFolder;
            }
            logger.debug(TAG, "getVirtualMapServiceOutputFolder.mapServerOutputFolder1: " + mapServerOutputFolder);
            String mapServiceSuffix = mapServiceName.replace("/", "_") + "_MapServer";
            String mapServiceOutputFolder = mapServerOutputFolder;
            if (mapServiceOutputFolder.endsWith("_MapServer") || mapServiceOutputFolder.endsWith("_MapServer/")) {
                //nothing to do, contains already the folder
            } else {
                //append mapService-folder to the url
                if (mapServiceSuffix.indexOf('_') != mapServiceSuffix.lastIndexOf('_')) {
                    //has folder/mapService structure
                    String msFolder = mapServiceSuffix.substring(mapServiceSuffix.indexOf('_') + 1);
                    logger.debug(TAG, "getVirtualMapServiceOutputFolder.msFolder: " + msFolder);
                    if (!mapServerOutputFolder.endsWith(msFolder) && !mapServerOutputFolder.substring(0, mapServiceOutputFolder.length() - 1).endsWith(msFolder)) {
                        mapServiceOutputFolder = mapServerOutputFolder + mapServiceSuffix;
                        logger.debug(TAG, "getVirtualMapServiceOutputFolder.mapServiceOutputFolder: " + mapServiceOutputFolder);
                    }
                } else {
                    mapServiceOutputFolder = mapServerOutputFolder + mapServiceName.replace("/", "_") + "_MapServer";
                }
            }
            logger.debug(TAG, "getVirtualMapServiceOutputFolder.mapServiceOutputFolder2: " + mapServiceOutputFolder);
            if (subFolder != null && subFolder.trim().length() > 0) {
                logger.debug(TAG, "getVirtualMapServiceOutputFolder.mapServiceOutputFolder.subFolder: " + subFolder);
                if (mapServiceOutputFolder.endsWith("/")) {
                    return mapServiceOutputFolder + subFolder;
                } else {
                    return mapServiceOutputFolder + "/" + subFolder;

                }
            }
            return mapServiceOutputFolder;
        } catch (IOException e) {
            logger.error(TAG, "getVirtualMapServiceOutputFolder.Exception", e);
            e.printStackTrace();
        }
        return "??";
    }

    public String getRequestContextURL() {
        if (requestContextURL != null) return requestContextURL;
        try {
            EnvironmentManager envMgr = new EnvironmentManager();
            UID envUID = new UID();
            envUID.setValue("{32d4c328-e473-4615-922c-63c108f55e60}");

            Object envObj = envMgr.getEnvironment(envUID);
            IServerEnvironment2 env2 = new IServerEnvironment2Proxy(envObj);

            IPropertySet propertySet = env2.getProperties();
            Object[] namesPara = new Object[1];
            Object[] valuesPara = new Object[1];
            propertySet.getAllProperties(namesPara, valuesPara);
            String[] namesStr = (String[]) namesPara[0];
            Object[] valuesObj = (Object[]) valuesPara[0];
            //String requestContextUrl = null;
            for (int i = 0; i < namesStr.length; i++) {
                String name = namesStr[i];
                logger.debug(TAG, "getRequestContextURL.property." + name + " = " + valuesObj[i]);
                if (name.equalsIgnoreCase("RequestContextURL")) {
                    requestContextURL = (String) valuesObj[i];
                }
            }
            if (requestContextURL != null) {
                logger.debug(TAG, "getRequestContextURL.requestContextUrl: " + requestContextURL);
                return requestContextURL;
            } else {
                logger.error(TAG, "getRequestContextURL.NO.requestContextUrl found");
                return "";
            }
        } catch (UnknownHostException e) {
            throw new RuntimeException(e.getMessage());
        } catch (AutomationException e) {
            throw new RuntimeException(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getMapServerOutputFolder(MapServer mapServer) throws IOException {
        String mapServerOutputFolder = mapServer.getPhysicalOutputDirectory();
        if (mapServerOutputFolder.contains("\\AppData\\Local\\Temp\\")) {
            //for local testing
            String localDir = "D:\\arcgisserver\\arcgisoutput\\";
            if (new File(localDir).isDirectory()) {
                mapServerOutputFolder = localDir;
            }
        }
        return mapServerOutputFolder;
    }

    public String getMapServiceMxdFile() throws IOException {
        String filePath = mapServer.getFilePath();
        logger.debug(TAG, "Got configFileName from mapServer: " + filePath);
        if (filePath == null || filePath.trim().length() == 0) {
            logger.warning(TAG, "FilePath not found in mapserver! Reading configuration file manually...");
            String arcgisServerHome = null;
            for (String agHome : possibleArcgisServerHomes) {
                File agHomeFile = new File(agHome);
                if (agHomeFile.canRead()) {
                    arcgisServerHome = agHome;
                    break;
                }

            }
            if (arcgisServerHome == null) {
                logger.error(TAG, "ArcGIS Server home not found !!!!");
            } else {
                logger.debug(TAG, "Using ArcGIS Server home: " + arcgisServerHome);

                String configFileName = arcgisServerHome + File.separator + mapServer.getConfigurationName() + ".MapServer.cfg";
                File configFile = new File(configFileName);
                if (configFile.canRead()) {
                    BufferedReader buffReader = new BufferedReader(new FileReader(configFile));
                    String configLine;
                    filePath = null;
                    while ((configLine = buffReader.readLine()) != null) {
                        if (configLine.indexOf("<FilePath>") > 0) {
                            filePath = configLine.substring(configLine.indexOf("<FilePath>") + 10);
                            filePath = filePath.substring(0, filePath.indexOf("</FilePath>"));
                            logger.debug(TAG, "FilePath found in configuration file: " + filePath);
                            break;
                        }
                    }
                    if (filePath != null && filePath.endsWith(".msd")) {
                        filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                        logger.warning(TAG, "Reading mxd file instead of msd file", filePath);
                    }
                } else {
                    logger.error(TAG, "Cannot read mapService-configuration: ", configFileName);
                }
            }
        } else {
            if (filePath.endsWith(".msd")) {
                logger.debug(TAG, "found MSD file: " + filePath);
                filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                logger.debug(TAG, "using MXD instead of MSD file: " + filePath);
            }
        }
        return filePath;
    }

    protected boolean initMapPageLayout(MapServer mapServer) {
        logger.warning(TAG, "initMapPageLayout... " + mapServer);
        try {
            String mapName = mapServer.getDefaultMapName();
            logger.debug(TAG, "initMapPageLayout.mapName", mapName);
            IMapServerInfo mapServerInfo = mapServer.getServerInfo(mapName);
            IMapLayerInfos layerInfos = mapServerInfo.getMapLayerInfos();
            int layerCount = layerInfos.getCount();
            logger.debug(TAG, "initMapPageLayout.layerCount", layerCount);

            String defaultMapName = mapServer.getDefaultMapName();
            logger.debug(TAG, "initMapPageLayout.defaultMapName", defaultMapName);
            try {
                map = (Map) mapServer.getMap(defaultMapName);
                pageLayout = (PageLayout) mapServer.getPageLayout();
            } catch (Exception ex) {
                logger.debug(TAG, "initMapPageLayout.Exception", ex.getMessage());
                String filePath = mapServer.getFilePath();
                if (filePath.endsWith(".msd")) {
                    filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                }
                if (filePath.startsWith("Z:\\opt\\arcgis")) {
                    filePath = filePath.replace("\\", "/");
                    filePath = filePath.substring(2);
                }
                logger.debug(TAG, "initMapPageLayout().usingMXDfile", filePath);
                File mxdFile = new File(filePath);
                if (!mxdFile.canRead()) {
                    logger.error(TAG, "initMapPageLayout().cannotRead: " + mxdFile);
                } else {
                    try {
                        MapServer newMapServer = new MapServer();
                        newMapServer.connect(filePath);
                        map = (Map) newMapServer.getMap(defaultMapName);
                        logger.debug(TAG, "initMapPageLayout().map", map);
                        pageLayout = (PageLayout) newMapServer.getPageLayout();
                        logger.debug(TAG, "initMapPageLayout().pageLayout", pageLayout);
                    } catch (Exception ex2) {
                        logger.error(TAG, "initMapPageLayout().newMapServer.getMap(defaultMapName).EXCEPTION: " + ex2.getMessage());
                    }
                }
            }
            logger.debug(TAG, "initMapPageLayout().SUCCESS");
            return true;
        } catch (AutomationException e) {
            logger.error(TAG, "initMapPageLayout()", e);
        } catch (IOException e) {
            logger.error(TAG, "initMapPageLayout()", e);
        }
        return false;
    }
}
