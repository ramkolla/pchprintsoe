/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.gisclient.IWMSLayerDescription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/23/11
 * Time: 6:40 AM
 */
public class LayerUtils {
    private static final String TAG = "LayerUtils";

    public static void setVisibilityForWMSLayerAndSubLayers(SOELogger logger, ILayer layer, boolean visibility) throws IOException {
        layer.setVisible(visibility);
        logger.debug(TAG, "setVisibilityForLayerAndSubLayers.layerVisible[" + layer.getName() + "] = " + visibility);
        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                setVisibilityForWMSLayerAndSubLayers(logger, compositeLayer.getLayer(i), visibility);
            }
        }
    }

    public static void setVisibilityForWMSLayerAndSubLayers(SOELogger logger, ILayer layer, List<String> visibleLayerList) throws IOException {
        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            layer.setVisible(true);
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                ILayer childLayer = compositeLayer.getLayer(i);
                logger.debug(TAG, "setVisibilityForWMSLayerAndSubLayers(" + childLayer.getName() + ")...");
                setVisibilityForWMSLayerAndSubLayers(logger, compositeLayer.getLayer(i), visibleLayerList);
            }
        } else {
            layer.setVisible(false);
            WMSLayer wmsLayer = (WMSLayer) layer;
            logger.debug(TAG, "setVisibilityForWMSLayerAndSubLayers(" + wmsLayer.getName() + ")...");
            IWMSLayerDescription wmsLayerDescription = wmsLayer.getWMSLayerDescription();
            if (visibleLayerList.indexOf(wmsLayerDescription.getName()) >= 0) {
                logger.debug(TAG, "setVisibilityForWMSLayerAndSubLayers(TRUE).wmsLyrDesc.name: " + wmsLayerDescription.getName() + " >> " + layer.getName());
                layer.setVisible(true);
            }
        }
    }

    public static int setVisibilityForAGSInternetSubLayers(ILayer layer, List<Integer> visibleLayerList, Integer currentId) throws IOException {
        if (layer instanceof IMapServerGroupLayer) {
            IMapServerGroupLayer compositeLayer = (IMapServerGroupLayer) layer;
            if (compositeLayer.getCount() > 0) {
                layer.setVisible(true);
            } else {
                layer.setVisible(visibleLayerList.contains(currentId));
            }
            currentId++;
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                currentId = setVisibilityForAGSInternetSubLayers(compositeLayer.getLayer(i), visibleLayerList, currentId);
            }
        } else {
            layer.setVisible(visibleLayerList.contains(currentId));
            currentId++;
        }

        return currentId;
    }

    public static List<ILayer> getMapLayers(SOELogger logger, IMap map, boolean removeLayerFromMap) throws IOException {
        List<ILayer> originalLayerList = new ArrayList<ILayer>();
        int layerCount = map.getLayerCount();
        while (layerCount > 0) {
            ILayer layer = map.getLayer(0);
            originalLayerList.add(layer);
            if (removeLayerFromMap) {
                logger.debug(TAG, "getMapLayers.removing Layer", layer.getName());
                map.deleteLayer(layer);
                layerCount = map.getLayerCount();
            } else {
                layerCount--;
            }
        }
        return originalLayerList;
    }

    public static void restoreMapLayers(SOELogger logger, IMap map, List<ILayer> originalLayerList, PageLayout pageLayout) throws IOException {
        map.clearLayers();
        while (map.getLayerCount() > 0) {
            ILayer layer = map.getLayer(0);
            logger.debug(TAG, "removing tempLayer", layer.getName());
            map.deleteLayer(layer);
        }
        Collections.reverse(originalLayerList);
        for (ILayer iLayer : originalLayerList) {
            logger.debug(TAG, "adding origLayer", iLayer.getName());
            map.addLayer(iLayer);
        }
    }

    public static HashMap<Integer, ILayer> getAllLayersById(SOELogger logger, MapServerLayer layer) {
        HashMap<Integer, ILayer> idLayerMap = new HashMap<Integer, ILayer>();
        try {
            for (int i = 0; i < layer.getCount(); i++) {
                ILayer subLayer = layer.getLayer(i);
                idLayerMap.putAll(getAllLayersById(logger, subLayer, idLayerMap));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return idLayerMap;
    }

    private static HashMap<Integer, ILayer> getAllLayersById(SOELogger logger, ILayer layer, HashMap<Integer, ILayer> idLayerMap) throws IOException {
        int currentId = idLayerMap.size();
        idLayerMap.put(currentId, layer);
        logger.debug(TAG, "getAllLayersById.layerId[" + currentId + "].name = " + layer.getName());
        if (layer instanceof IMapServerGroupLayer) {
            IMapServerGroupLayer compositeLayer = (IMapServerGroupLayer) layer;
            if (compositeLayer.getCount() > 0) {
                for (int i = 0; i < compositeLayer.getCount(); i++) {
                    ILayer subLayer = compositeLayer.getLayer(i);
                    idLayerMap.putAll(getAllLayersById(logger, subLayer, idLayerMap));
                }
            }
        }
        return idLayerMap;
    }

    public static HashMap<Integer, Integer> getParentLayerIds(SOELogger logger, MapServerLayer layer) {
        HashMap<Integer, Integer> parentsLayerIdMap = new HashMap<Integer, Integer>();
        try {
            for (int i = 0; i < layer.getCount(); i++) {
                ILayer subLayer = layer.getLayer(i);
                parentsLayerIdMap.putAll(getParentLayerIds(logger, subLayer, null, parentsLayerIdMap));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return parentsLayerIdMap;
    }

    private static HashMap<Integer, Integer> getParentLayerIds(SOELogger logger, ILayer layer, Integer parentLayerId, HashMap<Integer, Integer> parentsLayerIdMap) throws IOException {
        int currentId = parentsLayerIdMap.size();
        parentsLayerIdMap.put(currentId, parentLayerId);
        logger.debug(TAG, "getParentLayerIds.layerId[" + currentId + "].parentId = " + parentLayerId);
        if (layer instanceof IMapServerGroupLayer) {
            IMapServerGroupLayer compositeLayer = (IMapServerGroupLayer) layer;
            if (compositeLayer.getCount() > 0) {
                for (int i = 0; i < compositeLayer.getCount(); i++) {
                    ILayer subLayer = compositeLayer.getLayer(i);
                    parentsLayerIdMap.putAll(getParentLayerIds(logger, subLayer, currentId, parentsLayerIdMap));
                }
            }

        }
        return parentsLayerIdMap;
    }
}
