/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.display.LineCallout;
import com.esri.arcgis.display.SimpleFillSymbol;
import com.esri.arcgis.display.TextSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.INumberFormat;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.NumericFormat;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.UnitConverter;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;
import lu.etat.pch.gis.utils.json.format.AgsJsonNumericFormat;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 7, 2010
 * Time: 6:12:49 PM
 */
public class AgsJsonScaleText extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonScaleText";
    public static final String TYPE = "pchScaleText";
    private int style = esriScaleTextStyleEnum.esriScaleTextAbsolute; //esriScaleTextStyleEnum
    private AgsJsonNumericFormat numericFormat;
    private String mapUnitLabel;
    private String pageUnitLabel;
    private String mapUnits;
    private String pageUnits;
    private String separator;
    private AgsJsonRGBColor backgroundColor;
    private AgsJsonTextSymbol textSymbol;

    public AgsJsonScaleText(SOELogger logger, AgsJsonNumericFormat numericFormat) {
        super(TYPE, logger);
        this.numericFormat = numericFormat;
        this.separator = null;
    }

    public AgsJsonScaleText(SOELogger logger, String mapUnits, String pageUnits, String mapUnitLabel, String pageUnitLabel, AgsJsonNumericFormat numericFormat) {
        super(TYPE, logger);
        this.style = esriScaleTextStyleEnum.esriScaleTextRelative;
        this.mapUnits = mapUnits;
        this.pageUnits = pageUnits;
        this.mapUnitLabel = mapUnitLabel;
        this.pageUnitLabel = pageUnitLabel;
        this.numericFormat = numericFormat;
    }

    public AgsJsonScaleText(SOELogger logger, JSONObject jsonObject) {
        super(TYPE, logger);
        try {
            type = jsonObject.getString("type");
            if (!jsonObject.isNull("style")) style = jsonObject.getInt("style");
            if (!jsonObject.isNull("mapUnits")) mapUnits = jsonObject.getString("mapUnits");
            if (!jsonObject.isNull("mapUnitLabel")) mapUnitLabel = jsonObject.getString("mapUnitLabel");
            if (!jsonObject.isNull("pageUnits")) pageUnits = jsonObject.getString("pageUnits");
            if (!jsonObject.isNull("pageUnitLabel")) pageUnitLabel = jsonObject.getString("pageUnitLabel");
            if (!jsonObject.isNull("numericFormat")) {
                numericFormat = new AgsJsonNumericFormat(logger, jsonObject.getJSONObject("numericFormat"));
            }
            if (!jsonObject.isNull("separator")) separator = jsonObject.getString("separator");
            if (!jsonObject.isNull("backgroundColor")) {
                backgroundColor = new AgsJsonRGBColor(logger, jsonObject.getJSONObject("backgroundColor"));
            }
            if (!jsonObject.isNull("textSymbol"))
                this.textSymbol = new AgsJsonTextSymbol(logger, jsonObject.getJSONObject("textSymbol"));
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonScaleText(JSONObject)", e);
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("mapUnits", mapUnits);
        if (mapUnitLabel != null) symbObject.put("mapUnitLabel", mapUnitLabel);
        symbObject.put("pageUnits", pageUnits);
        if (pageUnitLabel != null) symbObject.put("pageUnitLabel", pageUnitLabel);
        symbObject.put("numericFormat", numericFormat.toJSON());
        symbObject.put("separator", separator);
        symbObject.put("textSymbol", textSymbol.toJSON());
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom) throws IOException {
        IUID scaleTextUID = new UID();
        scaleTextUID.setValue(ScaleText.getClsid());

        MapSurroundFrame scaleTextFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(scaleTextUID, null);
        ScaleText scaleText = (ScaleText) scaleTextFrame.getMapSurround();
        scaleText.setStyle(style);
        if (numericFormat != null) {
            INumberFormat numberFormat = scaleText.getNumberFormat();
            if (numberFormat instanceof NumericFormat) {
                scaleText.setNumberFormat(numericFormat.toArcObjects((NumericFormat) numberFormat));
            }
        }
        if (textSymbol != null) {
            scaleText.setSymbol((ITextSymbol) textSymbol.toArcObject().get(0));
        }
        if (backgroundColor != null) {
            if (scaleText.getSymbol() instanceof TextSymbol) {
                TextSymbol textSymbol = (TextSymbol) scaleText.getSymbol();
                LineCallout lineCallout = new LineCallout();
                SimpleFillSymbol simpleFillSymbol = new SimpleFillSymbol();
                simpleFillSymbol.setColor(backgroundColor.toArcObject());
                lineCallout.setBorderByRef(simpleFillSymbol);
                lineCallout.setLeaderLineByRef(null);
                lineCallout.setAccentBarByRef(null);
                textSymbol.setBackgroundByRef(lineCallout);

                scaleText.setSymbol(textSymbol);
            } else {
                logger.error(TAG, "AgsJsonScaleText.scaleText.getSymbol() is NOT a TextSymbol !!");
            }
        }
        if (mapUnits != null) scaleText.setMapUnits(UnitConverter.getEsriUnitFromString(logger, mapUnits));
        if (pageUnits != null) scaleText.setPageUnits(UnitConverter.getEsriUnitFromString(logger, pageUnits));
        if (mapUnitLabel != null) scaleText.setMapUnitLabel(mapUnitLabel);
        if (pageUnitLabel != null) scaleText.setPageUnitLabel(pageUnitLabel);
        if (separator != null)
            scaleText.setSeparator(separator);

        if (geom instanceof IPoint) {
            scaleTextFrame.setGeometry(geom);
            LayoutUtils.positionElement(logger, scaleTextFrame, (IPoint) geom, pageLayout.getScreenDisplay());
        } else
            scaleTextFrame.setGeometry(geom);
        return scaleTextFrame;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
