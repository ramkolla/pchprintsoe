/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ILineSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.GeometryUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonElement;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonEnvelope;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSimpleLineSymbol;
import lu.etat.pch.gis.utils.json.print.PchPrintMapService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 3/13/12
 * Time: 6:03 AM
 */
public class AgsJsonMapFrame extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonMapFrame";
    public static final String TYPE = "pchMapFrame";
    private AgsJsonEnvelope extent;
    private List<PchPrintMapService> mapServices = new ArrayList<PchPrintMapService>();
    private List<AgsJsonElement> mapElements = new ArrayList<AgsJsonElement>();
    private AgsJsonSimpleLineSymbol border;
    private Double referenceScale;
    private Double rotation;


    public AgsJsonMapFrame(SOELogger logger) {
        super(TYPE, logger);
    }

    public AgsJsonMapFrame(SOELogger logger, JSONObject jsonObject) {
        super(TYPE, logger);
        try {
            if (!jsonObject.isNull("extent")) extent = new AgsJsonEnvelope(logger, jsonObject.getJSONObject("extent"));
            if (!jsonObject.isNull("mapServices")) {
                Object msObj = jsonObject.get("mapServices");
                if (msObj instanceof JSONArray) {
                    JSONArray jsonArray = (JSONArray) msObj;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mapServices.add(new PchPrintMapService(jsonArray.getJSONObject(i)));
                    }
                } else if (msObj instanceof JSONObject) {
                    mapServices.add(new PchPrintMapService((JSONObject) msObj));
                }
            }
            if (!jsonObject.isNull("mapElements")) {
                JSONArray jsonArray = jsonObject.getJSONArray("mapElements");
                mapElements = new ArrayList<AgsJsonElement>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    mapElements.add(new AgsJsonElement(logger, jsonArray.getJSONObject(i)));
                }
            }
            if (!jsonObject.isNull("border"))
                border = new AgsJsonSimpleLineSymbol(logger, jsonObject.getJSONObject("border"));
            if (!jsonObject.isNull("referenceScale")) referenceScale = jsonObject.getDouble("referenceScale");
            if (!jsonObject.isNull("rotation")) rotation = jsonObject.getDouble("rotation");
        } catch (JSONException ex) {
            logger.error(TAG, "AgsJsonMapFrame(JSONObject).JSONException", ex);
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", TYPE);
        if (mapServices != null) {
            JSONArray array = new JSONArray();
            for (PchPrintMapService printMapService : mapServices)
                array.put(printMapService.toJSON());
            symbObject.put("mapServices", array);
        }
        if (extent != null) {
            symbObject.put("extent", extent.toJSON());
        }
        if (mapElements != null) {
            JSONArray array = new JSONArray();
            for (AgsJsonElement mapElement : mapElements)
                array.put(mapElement.toJSON());
            symbObject.put("mapElements", array);
        }
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom) throws IOException {
        logger.warning(TAG, "AgsJsonMapFrame.toArcObject.WRONG USAGE: no refererWebsite+authorization401 passed !!!!!");
        return toArcObject(pageLayout, mapFrame, geom, null, null);
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom, String refererWebsite, String authorization401) throws IOException {
        Map newMap = new Map();
        for (PchPrintMapService pchPrintMapService : mapServices) {
            ILayer layer = PchPrintMapService.getLayerForMapService(logger, pchPrintMapService, (short) 1, refererWebsite, null, authorization401);
            if (layer != null) {
                newMap.addLayer(layer);
            } else {
                logger.error(TAG, "AgsJsonMapFrame.toArcObject().layer IS-NULL.pchPrintMapService", pchPrintMapService);
            }
        }
        for (AgsJsonElement jsonMapElement : mapElements) {
            try {
                List<IElement> mapElements = jsonMapElement.toArcObject(logger);
                for (IElement mapElement : mapElements) {
                    if (mapElement != null) {
                        newMap.getGraphicsContainer().addElement(mapElement, 0);
                    } else {
                        logger.error(TAG, "AgsJsonMapFrame.toArcObjects.jsonMapElement.toArcObject()=NULL", jsonMapElement.toString());
                    }
                }
            } catch (Exception e) {
                logger.error(TAG, "AgsJsonMapFrame.toArcObjects.jsonMapElement.toArcObject(): " + jsonMapElement.toString(), e);
            }
        }
        MapFrame newMapFrame = new MapFrame();
        newMapFrame.setMapByRef(newMap);
        if (extent != null) newMap.setExtent(extent.toArcObject());
        newMap.refresh();
        //map.setMapScale(printOutput.calculateScale(extent.toArcObject(), map.getMapUnits()));
        newMap.refresh();
        newMapFrame.setMapByRef(newMap);
        if (geom instanceof IPoint) {
            newMapFrame.setGeometry(GeometryUtils.convertPoint2Envelope((IPoint) geom, 4d));
        } else {
            newMapFrame.setGeometry(geom);
        }
        return newMapFrame;
    }

    public AgsJsonEnvelope getExtent() {
        return extent;
    }

    public void setExtent(AgsJsonEnvelope extent) {
        this.extent = extent;
    }

    public List<PchPrintMapService> getMapServices() {
        return mapServices;
    }

    public void setMapServices(List<PchPrintMapService> mapServices) {
        this.mapServices = mapServices;
    }

    public List<AgsJsonElement> getMapElements() {
        return mapElements;
    }

    public void setMapElements(List<AgsJsonElement> mapElements) {
        this.mapElements = mapElements;
    }

    public AgsJsonSimpleLineSymbol getBorder() {
        return border;
    }

    public void setBorder(AgsJsonSimpleLineSymbol border) {
        this.border = border;
    }

    public Double getReferenceScale() {
        return referenceScale;
    }

    public void setReferenceScale(Double referenceScale) {
        this.referenceScale = referenceScale;
    }

    public Double getRotation() {
        return rotation;
    }

    public void setRotation(Double rotation) {
        this.rotation = rotation;
    }

    public void processMainMapFrame(MapFrame mapFrame, Map map) throws IOException {
        logger.debug(TAG, "processMainMapFrame....");
        if (border != null) {
            logger.debug(TAG, "processMainMapFrame.border", border.toJSON().toString());
            SymbolBorder simpleBorder = new SymbolBorder();
            simpleBorder.setLineSymbol((ILineSymbol) border.toArcObject().get(0));
            mapFrame.setBorder(simpleBorder);
        }
        if (referenceScale != null) {
            logger.debug(TAG, "processMainMapFrame.referenceScale", referenceScale);
            if (referenceScale == -1)
                map.setReferenceScale(map.getMapScale());
            else
                map.setReferenceScale(referenceScale);
        }
        if (rotation != null) {
            logger.debug(TAG, "processMainMapFrame.rotation", rotation);
            map.getScreenDisplay().getDisplayTransformation().setRotation(rotation);
        }
        logger.debug(TAG, "processMainMapFrame.DONE");
    }

}
