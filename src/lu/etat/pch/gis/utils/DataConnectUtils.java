/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.datasourcesGDB.AccessWorkspaceFactory;
import com.esri.arcgis.datasourcesGDB.FileGDBWorkspaceFactory;
import com.esri.arcgis.datasourcesraster.RasterDataset;
import com.esri.arcgis.datasourcesraster.RasterWorkspace;
import com.esri.arcgis.datasourcesraster.RasterWorkspaceFactory;
import com.esri.arcgis.geodatabase.FeatureClass;
import com.esri.arcgis.geodatabase.Workspace;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.gisclient.*;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.PropertySet;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 21, 2010
 * Time: 6:17:21 PM
 */
public class DataConnectUtils {
    public static ILayer createFileGeoDataBaseLayer(String geoDB, String featureClassName) throws IOException {
        FileGDBWorkspaceFactory fGDB = new FileGDBWorkspaceFactory();
        Workspace workspace = new Workspace(fGDB.openFromFile(geoDB, 0));
        FeatureClass fClass = new FeatureClass(workspace.openFeatureClass(featureClassName));
        FeatureLayer fLayer = new FeatureLayer();
        fLayer.setName(featureClassName);
        fLayer.setFeatureClassByRef(fClass);
        return fLayer;
    }

    public static ILayer createAccessGeoDataBaseLayer(String geoDB, String featureClassName) throws IOException {
        AccessWorkspaceFactory awFact = new AccessWorkspaceFactory();
        Workspace workspace = new Workspace(awFact.openFromFile(geoDB, 0));
        FeatureClass fClass = new FeatureClass(workspace.openFeatureClass(featureClassName));
        FeatureLayer fLayer = new FeatureLayer();
        fLayer.setName(featureClassName);
        fLayer.setFeatureClassByRef(fClass);
        return fLayer;
    }

    public static void printOutPropSet(IPropertySet propSet) {
        Object[] a = new Object[1];
        Object[] b = new Object[1];
        try {
            propSet.getAllProperties(a, b);
            Object[] aa = (Object[]) a[0];
            Object[] bb = (Object[]) b[0];
            for (int i = 0; i < bb.length; i++) {
                System.out.println(aa[i] + " = " + bb[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static ILayer createArcGISService(String url, String mapService) throws IOException {
        //Set up connection
        IAGSServerConnectionFactory connectionFactory = new AGSServerConnectionFactory();
        IPropertySet propertySet = new PropertySet();
        IAGSServerConnection connection;
        propertySet.setProperty("url", url);
        connection = connectionFactory.open(propertySet, 0);


        IAGSEnumServerObjectName serverObjectNames = connection.getServerObjectNames();
        serverObjectNames.reset();

        IAGSServerObjectName serverObjectName;
        while ((serverObjectName = serverObjectNames.next()) != null) {
            if (serverObjectName.getName().equals(mapService) && serverObjectName.getType().equals("MapServer")) {
                if (serverObjectName instanceof AGSServerObjectName) {
                    AGSServerObjectName name = (AGSServerObjectName) serverObjectName;
                    IPropertySet propertySet2 = name.getAGSServerConnectionName().getConnectionProperties();
                    Object[] a = new Object[propertySet.getCount()], b = new Object[propertySet.getCount()];
                    propertySet2.getAllProperties(a, b);
                    IMapServer mapServer = (IMapServer) name.open();
                    MapServerLayer layer = new MapServerLayer();
                    layer.serverConnect(serverObjectName, mapServer.getDefaultMapName());
                    return layer;
                }
            }
        }
        return null;
    }
    //todo: make a simpler AGS connection 

    public static void testAGS() throws IOException {
        MapServerLayer mapServerLayer = new MapServerLayer();
        AGSServerConnectionName connName = new AGSServerConnectionName();
        PropertySet propSet = new PropertySet();
        propSet.setProperty("url", "http://localhost:8399/arcgis/services");
        propSet.setProperty("HTTPTIMEOUT", 60);
        propSet.setProperty("MESSAGEFORMAT", 2);
        propSet.setProperty("TOKENSERVICEURL", "http://babifer:8399/arcgis/tokens");
        connName.setConnectionProperties(propSet);
        connName.open();
        mapServerLayer.connect(connName);
    }

    public static WMSMapLayer createWMSLayer(String wmsURL, short agsAlpha) throws IOException {
        WMSMapLayer wmsMapLayer = new WMSMapLayer();
        WMSConnectionName connName = new WMSConnectionName();
        IPropertySet propSet = new PropertySet();
        if(wmsURL.indexOf("/MapServer/WMSServer")>0) {
            wmsURL = wmsURL.substring(0,wmsURL.indexOf("/MapServer/WMSServer")+20);
        }
        propSet.setProperty("URL", wmsURL);
        propSet.setProperty("version", "1.3.0");
        connName.setConnectionProperties(propSet);
        wmsMapLayer.connect(connName);
        for (int i = 0; i < wmsMapLayer.getCount(); i++) {
            ILayer layer = wmsMapLayer.getLayer(i);
            SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
            IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
            ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
            layer.setSpatialReferenceByRef(spatialReference);
            layer.setVisible(true);
            if (layer instanceof WMSGroupLayer) {
                WMSGroupLayer wmsGroupLayer = (WMSGroupLayer) layer;
                for (int j = 0; j < wmsGroupLayer.getCount(); j++) {
                    ILayer childLayer = wmsGroupLayer.getLayer(j);
                    childLayer.setVisible(true);
                }
            }
        }
        wmsMapLayer.setTransparency(agsAlpha);
        return wmsMapLayer;
    }


    public static void main(String[] args) {
        try {
            LicenceUtils.initArcEngine();

            IActiveView activeView = null;
            String folderName = "d:\\";
            String rasterName ="flowDirection.tif";
            RasterWorkspaceFactory rasterWorkspaceFactory = new RasterWorkspaceFactory();
            RasterWorkspace inRasterWorkspace = new RasterWorkspace(rasterWorkspaceFactory.openFromFile(folderName, 0));
            RasterDataset inRasterDataset1 = (RasterDataset) inRasterWorkspace.openRasterDataset(rasterName);
            System.out.println("inRasterDataset1.getRasterInfo().getWidth() = " + inRasterDataset1.getRasterInfo().getWidth());
            System.out.println("inRasterDataset1.getRasterInfo().getHeight() = " + inRasterDataset1.getRasterInfo().getHeight());
            RasterLayer rasterLayer = new RasterLayer();
            rasterLayer.createFromDataset(inRasterDataset1);
            rasterLayer.setName("myLoadedRaster");
            activeView.getFocusMap().addLayer(rasterLayer);
            activeView.refresh();

            if (1 == 1) System.exit(0);
            FeatureClass fc = FeatureClassUtils.createFeatureClassFromArcSDEBasedDataSource("localhost", "5151", "sde", "sde", "sde", "DEFAULT", "dem5m2003");
            System.out.println("fc.getShapeFieldName() = " + fc.getShapeFieldName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ISpatialReference getSpatialReferenceLuxembourg() throws IOException {
        SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
        IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
        ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
        return spatialReference;
    }
}
